USE [Archive]
GO

/****** Object:  StoredProcedure [dbo].[Legacy_Import_PHINMS]    Script Date: 4/27/2017 7:56:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------
-- 
-- Name			 : Legacy_Import_PHINMS
-- Descscription : Based on based code for Legacy 
--
-- This process is currently configured for 3 parallel proesses.  It will need to be modified to one on as singular 
-- process
------------------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[Legacy_Import_PHINMS]
	@BatchSize	int  = 10000, 
	@N_Parallel INT  = 1,
	@Task_Number INT = 0,
	@Debug		 INT = 0
AS
BEGIN

-- This is the largest record value which contains a records within the specified data range.
	DECLARE @MaxRecord		BIGINT = 0
	--DECLARE @Begin_Date		DATETIME = '01/01/1900'
	--DECLARE @End_Date		DATETIME = '01/01/1900'

	DECLARE		@Site_Short_Name				VARCHAR(50) = 'PHINMS',
				--@Site_ID						INT,
				--@Str_Site_ID					VARCHAR(10),
				@LegacyTable					VARCHAR(50),
				@ArchiveTable					VARCHAR(50),
				@ProcessStatus					CHAR(1),
				@ProcessOrder					INT,
--				@BatchSize						INT,		-- Bob W. moved to parameter
				@Tracking_Table_Name			VARCHAR(128)
					

	DECLARE		@Archive_Processed_Table_Name	VARCHAR(50),
				@Import_Start					DATETIME,
				@Import_Stop					DATETIME,
				@Imported_Rows					INT = 0,
				@Process_Status					VARCHAR(25),
				@Process_Msg					VARCHAR(2000),
				@MyRC							INT

	DECLARE		@StartID						BIGINT,
				@EndID							BIGINT

	DECLARE		@Str_StartID					VARCHAR(50),
				@Str_BatchSize					VARCHAR(50)

	DECLARE		@TSQL_Cmd						VARCHAR(2000)

	--DECLARE		@Source_Server					VARCHAR(50)	='[DataMart].[BioSense].[dbo].[XX_Stage_1_Archive]',
	--			@Dest_Server					VARCHAR(50)	='[ARCHIVE]'

	DECLARE		@StartCtr						BIGINT,
				@Ctr							BIGINT,
				@LCtr							BIGINT,
				@MyRow							BIGINT

	DECLARE @Test_cfacilityid				VARCHAR(100),
			@Test_cfacilityidsource			VARCHAR(100),
			@Test_cmftpatientclass			VARCHAR(100),
			@Test_cbiosensefacilityid		VARCHAR(100),
			@Test_sendingfacilityid			VARCHAR(100),
			@Test_sendingfacilityidsource	VARCHAR(100)

	DECLARE @Test_SiteID					INT,
			@Test_MessageID					BIGINT,
			@Test_CUniquePatientID			VARCHAR(100),
			@Test_CVisitDateTime			DATETIME,
			@Test_treatingfacilityid		VARCHAR(100),
			@Test_CPatientClass				VARCHAR(3)

	DECLARE
			@Output_FacilityID_UUID			VARCHAR(80),
			@Input_FacilityID_UUID			VARCHAR(80),
			@Patient_Class_Code				CHAR(1),
			@C_Biosense_Facility_ID			INT,
			@EVN_7_2_Event_Facility			VARCHAR(200),
			@MSH_4_Sending_Facility			VARCHAR(200),
			@C_FacilityIDSource				VARCHAR(10)


	CREATE TABLE #TempTable
	(
		[Processed_ID] [bigint] NULL,
		[Message_ID] [bigint] NOT NULL,
		[Str_Arrived_Date_Time] [varchar](24) NOT NULL,
		[Arrived_Date_Time] [datetime2](0) NOT NULL,
		[Create_Raw_Date_Time] [datetime2](7) NOT NULL,
		[Feed_Name] [varchar](80) NOT NULL,
		[Channel_Name] [varchar](80) NULL,
		[Sending_Application] [varchar](200) NULL,
		[Receiving_Application] [varchar](200) NULL,
		[Receiving_Facility] [varchar](200) NULL,
		[Message_Type] [varchar](15) NULL,
		[Trigger_Event] [varchar](15) NULL,
		[Message_Structure] [varchar](15) NULL,
		[Message_Control_ID] [varchar](200) NULL,
		[Processing_ID] [varchar](15) NULL,
		[Version_ID] [varchar](15) NULL,
		[Message_Profile_ID] [varchar](500) NULL,
		[Site_ID] [varchar](80) NULL,
		[Admit_Reason_Code] [varchar](255) NULL,
		[Admit_Reason_Description] [varchar](3000) NULL,
		[Admit_Reason_Segment] [varchar](8000) NULL,
		[Admit_Reason_Combo] [varchar](8000) NULL,
		[Chief_Complaint_Code] [varchar](255) NULL,
		[Chief_Complaint_Text] [varchar](3000) NULL,
		[Chief_Complaint_Segment] [varchar](8000) NULL,
		[Chief_Complaint_Combo] [varchar](8000) NULL,
		[Chief_Complaint_Type] [varchar](20) NULL,
		[C_Chief_Complaint] [varchar](8000) NULL,
		[C_Chief_Complaint_Source] [varchar](200) NULL,
		[Str_Age_Calculated] [varchar](10) NULL,
		[Age_Calculated] [decimal](6, 2) NULL,
		[Age_Units_Calculated] [varchar](50) NULL,
		[Str_Age_Reported] [varchar](10) NULL,
		[Age_Reported] [decimal](6, 2) NULL,
		[Age_Units_Reported] [varchar](50) NULL,
		[Str_Birth_Date_Time] [varchar](100) NULL,
		[Birth_Date_Time] [datetime2](0) NULL,
		[C_Patient_Age] [varchar](10) NULL,
		[C_Patient_Age_Units] [varchar](50) NULL,
		[C_Patient_Age_Years] [int] NULL,
		[C_Patient_Age_Source] [varchar](15) NULL,
		[Patient_Class_Code] [varchar](20) NULL,
		[Facility_Type_Code] [varchar](50) NULL,
		[Facility_Type_Description] [varchar](600) NULL,
		[Facility_Type_Segment] [varchar](800) NULL,
		[C_FacType_Patient_Class] [varchar](3) NULL,
		[C_Patient_Class] [varchar](3) NULL,
		[C_Patient_Class_Source] [varchar](3) NULL,
		[Treating_Facility_ID] [varchar](200) NULL,
		[Sending_Facility_ID] [varchar](200) NULL,
		[C_Facility_ID] [varchar](200) NULL,
		[Sending_Facility_ID_Source] [varchar](10) NULL,
		[C_Facility_ID_Source] [varchar](10) NULL,
		[C_Processed_Facility_ID] [varchar](300) NULL,
		[Str_Admit_Date_Time] [varchar](24) NULL,
		[Admit_Date_Time] [datetime2](0) NULL,
		[Str_Discharge_Date_Time] [varchar](24) NULL,
		[Discharge_Date_Time] [datetime2](0) NULL,
		[Str_Observation_Date_Time] [varchar](24) NULL,
		[Observation_Date_Time] [datetime2](0) NULL,
		[Str_Procedure_Date_Time] [varchar](24) NULL,
		[Procedure_Date_Time] [datetime2](0) NULL,
		[Str_Death_Date_Time] [varchar](24) NULL,
		[Death_Date_Time] [datetime2](0) NULL,
		[Str_Recorded_Date_Time] [varchar](24) NULL,
		[Recorded_Date_Time] [datetime2](0) NULL,
		[Str_Message_Date_Time] [varchar](24) NULL,
		[Message_Date_Time] [datetime2](0) NULL,
		[Str_Diagnosis_Date_Time] [varchar](24) NULL,
		[Diagnosis_Date_Time] [datetime2](0) NULL,
		[C_Visit_Date_Time] [datetime2](0) NULL,
		[C_Visit_Date] [date] NULL,
		[C_Visit_Date_Source] [varchar](15) NULL,
		[First_Patient_ID] [varchar](100) NULL,
		[First_Patient_ID_Assigning_Authority] [varchar](200) NULL,
		[First_Patient_ID_Assigning_Facility] [varchar](200) NULL,
		[First_Patient_ID_Type_Code] [varchar](3) NULL,
		[Patient_Account_Number] [varchar](100) NULL,
		[Visit_ID] [varchar](100) NULL,
		[Visit_ID_Assigning_Authority] [varchar](200) NULL,
		[Visit_ID_Assigning_Facility] [varchar](200) NULL,
		[Medical_Record_Number] [varchar](100) NULL,
		[Medical_Record_Number_Assigning_Authority] [varchar](200) NULL,
		[Medical_Record_Number_Assigning_Facility] [varchar](200) NULL,
		[C_Unique_Patient_ID] [varchar](100) NULL,
		[C_Unique_Patient_ID_Source] [varchar](20) NULL,
		[Patient_Death_Indicator] [varchar](20) NULL,
		[C_Death] [varchar](3) NULL,
		[C_Death_Source] [varchar](15) NULL,
		[C_BioSense_ID] [varchar](400) NULL,
		[C_Processed_BioSense_ID] [varchar](500) NULL,
		[Patient_Zip] [varchar](10) NULL,
		[C_Patient_County] [varchar](100) NULL,
		[C_Patient_County_Source] [varchar](10) NULL,
		[Diagnosis_Code] [varchar](255) NULL,
		[Diagnosis_Description] [varchar](3000) NULL,
		[Diagnosis_Segment] [varchar](8000) NULL,
		[Diagnosis_Combo] [varchar](8000) NULL,
		[Diagnosis_Type] [varchar](100) NULL,
		[Diagnosis_Priority] [varchar](100) NULL,
		[Discharge_Disposition] [varchar](255) NULL,
		[Triage_Notes] [varchar](8000) NULL,
		[Administrative_Sex] [varchar](20) NULL,
		[Race_Code] [varchar](100) NULL,
		[Race_Description] [varchar](600) NULL,
		[Race_Segment] [varchar](800) NULL,
		[Ethnicity_Code] [varchar](100) NULL,
		[Ethnicity_Description] [varchar](600) NULL,
		[Ethnicity_Segment] [varchar](800) NULL,
		[Str_Initial_Temp] [varchar](20) NULL,
		[Initial_Temp] [decimal](6, 2) NULL,
		[Initial_Temp_Units] [varchar](50) NULL,
		[Alternate_Visit_ID] [varchar](100) NULL,
		[Procedure_Code] [varchar](255) NULL,
		[Procedure_Description] [varchar](3000) NULL,
		[Procedure_Segment] [varchar](8000) NULL,
		[Procedure_Combo] [varchar](8000) NULL,
		[Str_Onset_Date] [varchar](24) NULL,
		[Onset_Date] [datetime2](0) NULL,
		[Clinical_Impression] [varchar](8000) NULL,
		[Problem_List_Code] [varchar](255) NULL,
		[Problem_List_Description] [varchar](3000) NULL,
		[Problem_List_Segment] [varchar](8000) NULL,
		[Problem_List_Combo] [varchar](8000) NULL,
		[Str_Initial_Pulse_Oximetry] [varchar](10) NULL,
		[Initial_Pulse_Oximetry] [int] NULL,
		[Initial_Pulse_Oximetry_Units] [varchar](50) NULL,
		[Initial_Acuity_Code] [varchar](255) NULL,
		[Initial_Acuity_Description] [varchar](3000) NULL,
		[Initial_Acuity_Segment] [varchar](8000) NULL,
		[Initial_Acuity_Combo] [varchar](8000) NULL,
		[Servicing_Facility] [varchar](200) NULL,
		[Unique_Physician_Identifier] [varchar](100) NULL,
		[Unique_Physician_Identifier_Assigning_Authority] [varchar](200) NULL,
		[Provider_Type_Code] [varchar](50) NULL,
		[Provider_Type_Description] [varchar](600) NULL,
		[Provider_Type_Segment] [varchar](800) NULL,
		[Provider_Type_Combo] [varchar](800) NULL,
		[Patient_City] [varchar](80) NULL,
		[Patient_State] [varchar](50) NULL,
		[Patient_Country] [varchar](50) NULL,
		[Hospital_Unit_Code] [varchar](50) NULL,
		[Hospital_Unit_Description] [varchar](600) NULL,
		[Hospital_Unit_Segment] [varchar](800) NULL,
		[Str_Height] [varchar](10) NULL,
		[Height] [decimal](6, 2) NULL,
		[Height_Units] [varchar](50) NULL,
		[Str_Weight] [varchar](10) NULL,
		[Weight] [decimal](6, 2) NULL,
		[Weight_Units] [varchar](50) NULL,
		[Str_Body_Mass_Index] [varchar](10) NULL,
		[Body_Mass_Index] [int] NULL,
		[Smoking_Status_Code] [varchar](50) NULL,
		[Smoking_Status_Description] [varchar](600) NULL,
		[Smoking_Status_Segment] [varchar](800) NULL,
		[Str_Systolic_Blood_Pressure] [varchar](10) NULL,
		[Systolic_Blood_Pressure] [int] NULL,
		[Systolic_Blood_Pressure_Units] [varchar](50) NULL,
		[Str_Diastolic_Blood_Pressure] [varchar](10) NULL,
		[Diastolic_Blood_Pressure] [int] NULL,
		[Diastolic_Blood_Pressure_Units] [varchar](50) NULL,
		[Str_Systolic_Diastolic_Blood_Pressure] [varchar](20) NULL,
		[Systolic_Diastolic_Blood_Pressure] [int] NULL,
		[Systolic_Diastolic_Blood_Pressure_Units] [varchar](100) NULL,
		[Event_Type_Code] [varchar](20) NULL,
		[Insurance_Coverage] [varchar](20) NULL,
		[Insurance_Company_ID] [varchar](100) NULL,
		[Medication_List] [varchar](8000) NULL,
		[Medication_Code] [varchar](50) NULL,
		[Medication_Description] [varchar](600) NULL,
		[Medication_Segment] [varchar](800) NULL,
		[Medication_Combo] [varchar](800) NULL,
		[Previous_Hospital_Unit] [varchar](80) NULL,
		[Pregnancy_Status_Code] [varchar](50) NULL,
		[Pregnancy_Status_Description] [varchar](600) NULL,
		[Pregnancy_Status_Segment] [varchar](800) NULL,
		[Hospital_Service] [varchar](500) NULL,
		[Admit_Source] [varchar](15) NULL,
		[Ambulatory_Status] [varchar](50) NULL,
		[Admission_Type] [varchar](15) NULL,
		[Travel_History] [varchar](8000) NULL,
		[Discharge_Instructions] [varchar](8000) NULL,
		[Initial_Evaluation_Note] [varchar](8000) NULL,
		[C_MFT_Patient_Class] [varchar](10) NULL,
		[Time_Zone] [varchar](6) NULL,
		[Create_Processed_Date_Time] [datetime2](7) NOT NULL,
		[Update_Processed_Date_Time] [datetime2](7) NULL,
		[Update_Essence] [char](1) NOT NULL DEFAULT ('N'),
		[Assigned_Patient_Location] [varchar](100) NULL,
		[Legacy_Flag] [char](1) NULL,
		[Legacy_Row_Number] [bigint] NULL,
		[C_Biosense_Facility_ID] [int] NULL,
		[C_BMI] [decimal](6, 2) NULL,
----
		[Output_FacilityID_UUID]		VARCHAR(80) NULL,
		[Input_FacilityID_UUID]			VARCHAR(80) NULL,
--		[Patient_Class_Code]				CHAR(1) NULL,
--		[C_Biosense_Facility_ID]			INT NULL,
		[EVN_7_2_Event_Facility]		VARCHAR(200) NULL,
		[MSH_4_Sending_Facility]		VARCHAR(200) NULL,
		[Hold_Site_ID]					int NULL, -- Temp holder for Site_ID
		[EVN_MSH_Resolved]				BIT NULL

--		[C_Facility_ID_Source]				VARCHAR(10) NULL
		)


	DECLARE @Control_Table TABLE
	(
		Row_Number	bigint PRIMARY KEY
	)

	DECLARE @Exceptions_Count table
		(Message_ID BIGINT, reason_code varchar(2) );

	DECLARE @Exception_Count	BIGINT = 0
	DECLARE @RC					INT
	DECLARE @ErrorCode			INT

	SET NOCOUNT ON	

	-- Bob W. - With this beeing a one of process, the values will be set in code to reduce any outside dependencies on tables
	--SELECT @Site_ID = Site_ID, @LegacyTable = Legacy_Processed_Table_Name, @ArchiveTable = Archive_Processed_Table_Name, @ProcessStatus = Process_Status, @ProcessOrder = Process_Order, @StartID = LastProcessedID, @BatchSize = Process_Batch_Size
	--	FROM [dbo].[Legacy_Process_Control] WHERE Site_Short_Name = @Site_Short_Name

	--SET 	@Site_ID		= NULL -- Set for each row
	SET		@LegacyTable	= 'PH_Stage_1_Archive'
	SET		@ArchiveTable	= 'PH_Staging_Processed'
	SET 	@StartID		= 0


	-- Populate to control table to identity records to be processed this pass
	--  *Note:  The value for @Resource is case sensitive	
	IF (@Debug = 1)
		PRINT 'Starting Load of Control Table'

	INSERT @Control_Table ([Row_Number])
		SELECT TOP (@BatchSize) a.[Row_NUMBER] FROM [Legacy_Biosense].[dbo].[PH_Stage_1_Archive] a (NOLOCK)
--			JOIN [Legacy_Biosense].[dbo].[PH_TestSet] t (NOLOCK) ON a.[Row_Number] = t.[Row_Number]
		WHERE (a.[Parallel_Row_ID] = @Task_Number) AND (a.Convert_Status = 'N')
		OPTION (MAXDOP 1)
-- Bob W. Taken out due to lag time for retrieving records		WHERE a.Convert_Status = 'N' AND (a.[Row_Number] % @N_Parallel = @Task_Number)

	-- End of this code for the blocking to handle concurrent processing

	--SELECT TOP (@BatchSize) @StartID = MIN([a.Row_Number]), @EndID = MIN([a.Row_Number])
	--	FROM [DataMart].[BioSense].[dbo].[XX_Stage_1_Archive] a WITH (NOLOCK) 
	--		JOIN [Legacy_BioSense].[dbo].[PH_TestSet] t(NOLOCK) ON a.[Row_Number] = t.[Row_Number]
-- Bob W. based on date currently		WHERE Create_Date_Time >= @Begin_Date AND Create_Date_Time < @End_Date

--	SET @EndID = @StartID + @BatchSize - 1

--	SET @Str_Site_ID = LTRIM(RTRIM(STR(@Site_ID)))

--SELECT * FROM @Control_Table	

	IF (@Debug = 1)
		PRINT 'Starting Loop'

	SET @Import_Start = SYSDATETIME()


		BEGIN TRY
			IF (@Debug = 1)
				PRINT 'Inside Try Structure'

			INSERT #TempTable
			([Administrative_Sex]
			,[Admission_Type]
			,[Admit_Date_Time]
			,[Admit_Reason_Code]
			,[Admit_Reason_Combo]
			,[Admit_Reason_Description]
			,[Admit_Reason_Segment]
			,[Admit_Source]
			,[Age_Calculated]
			,[Age_Reported]
			,[Age_Units_Calculated]
			,[Age_Units_Reported]
			,[Alternate_Visit_ID]
			,[Ambulatory_Status]
			,[Arrived_Date_Time]
			,[Assigned_Patient_Location]
			,[Birth_Date_Time]
			,[Body_Mass_Index]
			,[C_Biosense_Facility_ID]
			,[C_BioSense_ID]
			,[C_BMI]
			,[C_Chief_Complaint]
			,[C_Chief_Complaint_Source]
			,[C_Death]
			,[C_Death_Source]
			,[C_Facility_ID]
			,[C_Facility_ID_Source]
			,[C_FacType_Patient_Class]
			,[C_MFT_Patient_Class]
			,[C_Patient_Age]
			,[C_Patient_Age_Source]
			,[C_Patient_Age_Units]
			,[C_Patient_Age_Years]
			,[C_Patient_Class]
			,[C_Patient_Class_Source]
			,[C_Patient_County]
			,[C_Patient_County_Source]
			,[C_Processed_BioSense_ID]
			,[C_Processed_Facility_ID]
			,[C_Unique_Patient_ID]
			,[C_Unique_Patient_ID_Source]
			,[C_Visit_Date]
			,[C_Visit_Date_Source]
			,[C_Visit_Date_Time]
			,[Channel_Name]
			,[Chief_Complaint_Code]
			,[Chief_Complaint_Combo]
			,[Chief_Complaint_Segment]
			,[Chief_Complaint_Text]
			,[Chief_Complaint_Type]
			,[Clinical_Impression]
			,[Create_Processed_Date_Time]
			,[Create_Raw_Date_Time]
			,[Death_Date_Time]
			,[Diagnosis_Code]
			,[Diagnosis_Combo]
			,[Diagnosis_Date_Time]
			,[Diagnosis_Description]
			,[Diagnosis_Priority]
			,[Diagnosis_Segment]
			,[Diagnosis_Type]
			,[Diastolic_Blood_Pressure]
			,[Diastolic_Blood_Pressure_Units]
			,[Discharge_Date_Time]
			,[Discharge_Disposition]
			,[Discharge_Instructions]
			,[Ethnicity_Code]
			,[Ethnicity_Description]
			,[Ethnicity_Segment]
			,[Event_Type_Code]
			,[Facility_Type_Code]
			,[Facility_Type_Description]
			,[Facility_Type_Segment]
			,[Feed_Name]
			,[First_Patient_ID]
			,[First_Patient_ID_Assigning_Authority]
			,[First_Patient_ID_Assigning_Facility]
			,[First_Patient_ID_Type_Code]
			,[Height]
			,[Height_Units]
			,[Hospital_Service]
			,[Hospital_Unit_Code]
			,[Hospital_Unit_Description]
			,[Hospital_Unit_Segment]
			,[Initial_Acuity_Code]
			,[Initial_Acuity_Combo]
			,[Initial_Acuity_Description]
			,[Initial_Acuity_Segment]
			,[Initial_Evaluation_Note]
			,[Initial_Pulse_Oximetry]
			,[Initial_Pulse_Oximetry_Units]
			,[Initial_Temp]
			,[Initial_Temp_Units]
			,[Insurance_Company_ID]
			,[Insurance_Coverage]
			,[Legacy_Flag]
			,[Legacy_Row_Number]
			,[Medical_Record_Number]
			,[Medical_Record_Number_Assigning_Authority]
			,[Medical_Record_Number_Assigning_Facility]
			,[Medication_Code]
			,[Medication_Combo]
			,[Medication_Description]
			,[Medication_List]
			,[Medication_Segment]
			,[Message_Control_ID]
			,[Message_Date_Time]
			,[Message_ID]  -- Uses Default value to get next sequence value
			,[Message_Profile_ID]
			,[Message_Structure]
			,[Message_Type]
			,[Observation_Date_Time]
			,[Onset_Date]
			,[Patient_Account_Number]
			,[Patient_City]
			,[Patient_Class_Code]
			,[Patient_Country]
			,[Patient_Death_Indicator]
			,[Patient_State]
			,[Patient_Zip]
			,[Pregnancy_Status_Code]
			,[Pregnancy_Status_Description]
			,[Pregnancy_Status_Segment]
			,[Previous_Hospital_Unit]
			,[Problem_List_Code]
			,[Problem_List_Combo]
			,[Problem_List_Description]
			,[Problem_List_Segment]
			,[Procedure_Code]
			,[Procedure_Combo]
			,[Procedure_Date_Time]
			,[Procedure_Description]
			,[Procedure_Segment]
			--,[Processed_ID]
			,[Processing_ID]
			,[Provider_Type_Code]
			,[Provider_Type_Combo]
			,[Provider_Type_Description]
			,[Provider_Type_Segment]
			,[Race_Code]
			,[Race_Description]
			,[Race_Segment]
			,[Receiving_Application]
			,[Receiving_Facility]
			,[Recorded_Date_Time]
			,[Sending_Application]
			,[Sending_Facility_ID]
			,[Sending_Facility_ID_Source]
			,[Servicing_Facility]
			,[Site_ID]
			,[Smoking_Status_Code]
			,[Smoking_Status_Description]
			,[Smoking_Status_Segment]
			,[Str_Admit_Date_Time]
			,[Str_Age_Calculated]
			,[Str_Age_Reported]
			,[Str_Arrived_Date_Time]
			,[Str_Birth_Date_Time]
			,[Str_Body_Mass_Index]
			,[Str_Death_Date_Time]
			,[Str_Diagnosis_Date_Time]
			,[Str_Diastolic_Blood_Pressure]
			,[Str_Discharge_Date_Time]
			,[Str_Height]
			,[Str_Initial_Pulse_Oximetry]
			,[Str_Initial_Temp]
			,[Str_Message_Date_Time]
			,[Str_Observation_Date_Time]
			,[Str_Onset_Date]
			,[Str_Procedure_Date_Time]
			,[Str_Recorded_Date_Time]
			,[Str_Systolic_Blood_Pressure]
			,[Str_Systolic_Diastolic_Blood_Pressure]
			,[Str_Weight]
			,[Systolic_Blood_Pressure]
			,[Systolic_Blood_Pressure_Units]
			,[Systolic_Diastolic_Blood_Pressure]
			,[Systolic_Diastolic_Blood_Pressure_Units]
			,[Time_Zone]
			,[Travel_History]
			,[Treating_Facility_ID]
			,[Triage_Notes]
			,[Trigger_Event]
			,[Unique_Physician_Identifier]
			,[Unique_Physician_Identifier_Assigning_Authority]
			,[Update_Essence]
			,[Update_Processed_Date_Time]
			,[Version_ID]
			,[Visit_ID]
			,[Visit_ID_Assigning_Authority]
			,[Visit_ID_Assigning_Facility]
			,[Weight]
			,[Weight_Units]
			,[Output_FacilityID_UUID]
			,[Input_FacilityID_UUID]
			,[EVN_7_2_Event_Facility]
			,[MSH_4_Sending_Facility]
			,[Hold_Site_ID]
			,[EVN_MSH_Resolved]
			)
			SELECT 
				 a.[PID_8_1_Patient_Gender] "Administrative_Sex"
				,a.[PV1_4_1_Admission_Type] "Admission_Type"
				,CASE 
					WHEN TRY_PARSE(a.[PV1_44_1_Admit_Date_Time] AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[PV1_44_1_Admit_Date_Time] AS DateTime2)
					WHEN [dbo].[Parse_HL7_Date_to_Datetime](a.[PV1_44_1_Admit_Date_Time],'I') IS NOT NULL THEN [dbo].[Parse_HL7_Date_to_Datetime](a.[PV1_44_1_Admit_Date_Time],'I') 	
					WHEN TRY_PARSE(a.[PV1_44_1_Admit_Date_Time]+':00' AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[PV1_44_1_Admit_Date_Time]+':00' AS DateTime2)
					--WHEN LEN(a.[PV1_44_1_Admit_Date_Time])=6 THEN TRY_PARSE(a.[PV1_44_1_Admit_Date_Time]+'01' as datetime2)
					ELSE NULL
				 END "Admit_Date_Time"
--				,TRY_PARSE((dbo.[legacy_Simple_Str2Date](a.[PV1_44_1_Admit_Date_Time])) AS DATETIME) "Admit_Date_Time"
				,REPLACE(a.[PV2_3_1_Admit_Reason_ID],':',';') "Admit_Reason_Code"
				,NULL "Admit_Reason_Combo"
				,CASE 
					WHEN LEN(RTRIM(a.[PV2_3_2_Admit_Reason_Text])) > 0 THEN REPLACE(a.[PV2_3_2_Admit_Reason_Text],':',';') 
					ELSE REPLACE(a.[PV2_3_5_Admit_Reason_Alt_Text],':',';')  
				 END "Admit_Reason_Description"
				,NULL "Admit_Reason_Segment"
				,a.[PV1_14_1_Admit_Source] "Admit_Source"
--				,REPLACE(a.[OBX_5_1_Patient_Age_Calculated],':',';')  "Age_Calculated"
--				,REPLACE(a.[OBX_5_1_Patient_Age_Reported],':',';')  "Age_Reported"
				,CASE
					WHEN CHARINDEX(':',a.[OBX_5_1_Patient_Age_Calculated])>0 THEN TRY_PARSE([dbo].[Get_First_Value](a.[OBX_5_1_Patient_Age_Calculated],':') AS decimal(6,2)) 
					WHEN CHARINDEX(';',a.[OBX_5_1_Patient_Age_Calculated])>0 THEN TRY_PARSE([dbo].[Get_First_Value](a.[OBX_5_1_Patient_Age_Calculated],';') AS decimal(6,2)) 
					ELSE TRY_PARSE(a.[OBX_5_1_Patient_Age_Calculated] AS decimal(6,2))
				 END "Age_Calculated"
				,CASE
					WHEN CHARINDEX(':',a.[OBX_5_1_Patient_Age_Reported])>0 THEN TRY_PARSE([dbo].[Get_First_Value](a.[OBX_5_1_Patient_Age_Reported],':') AS decimal(6,2))  
					WHEN CHARINDEX(';',a.[OBX_5_1_Patient_Age_Reported])>0 THEN TRY_PARSE([dbo].[Get_First_Value](a.[OBX_5_1_Patient_Age_Reported],';') AS decimal(6,2))  
					ELSE TRY_PARSE(a.[OBX_5_1_Patient_Age_Reported] AS decimal(6,2))
				 END "Age_Reported"
				----,CASE
				----	WHEN CHARINDEX(':',a.[OBX_5_1_Patient_Age_Calculated])>0 THEN TRY_PARSE([dbo].[Get_First_Value](a.[OBX_5_1_Patient_Age_Calculated],':') AS INT) 
				----	WHEN CHARINDEX(';',a.[OBX_5_1_Patient_Age_Calculated])>0 THEN TRY_PARSE([dbo].[Get_First_Value](a.[OBX_5_1_Patient_Age_Calculated],';') AS INT) 
				----	ELSE TRY_PARSE(a.[OBX_5_1_Patient_Age_Calculated] AS INT)
				---- END "Age_Calculated"
				----,CASE
				----	WHEN CHARINDEX(':',a.[OBX_5_1_Patient_Age_Reported])>0 THEN TRY_PARSE([dbo].[Get_First_Value](a.[OBX_5_1_Patient_Age_Reported],':') AS INT)  
				----	WHEN CHARINDEX(';',a.[OBX_5_1_Patient_Age_Reported])>0 THEN TRY_PARSE([dbo].[Get_First_Value](a.[OBX_5_1_Patient_Age_Reported],';') AS INT)  
				----	ELSE TRY_PARSE(a.[OBX_5_1_Patient_Age_Reported] AS INT)
				---- END "Age_Reported"
				,REPLACE(a.[OBX_6_2_Patient_Age_Calculated_U],':',';')  "Age_Units_Calculated"
				,REPLACE(a.[OBX_6_2_Patient_Age_Reported_Uni],':',';')  "Age_Units_Reported"
				,a.[PV1_50_1_Alternate_Visit_ID] "Alternate_Visit_ID"
				,REPLACE(a.[PV1_15_1_Ambulatory_Status],':',';')  "Ambulatory_Status"
				,TRY_CONVERT(DATETIME,a.[Create_Date_Time]) "Arrived_Date_Time" 
				,NULL "Assigned_Patient_Location"
				,CASE 
					WHEN TRY_PARSE(a.[PID_7_1_Date_Time_of_Birth] AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[PID_7_1_Date_Time_of_Birth] AS DateTime2)
					WHEN [dbo].[Parse_HL7_Date_to_Datetime](a.[PID_7_1_Date_Time_of_Birth],'I') IS NOT NULL THEN [dbo].[Parse_HL7_Date_to_Datetime](a.[PID_7_1_Date_Time_of_Birth],'I') 	
					WHEN TRY_PARSE(a.[PID_7_1_Date_Time_of_Birth]+':00' AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[PID_7_1_Date_Time_of_Birth]+':00' AS DateTime2)
					--WHEN LEN(a.[PID_7_1_Date_Time_of_Birth])=6 THEN TRY_PARSE(a.[PID_7_1_Date_Time_of_Birth]+'01' as datetime2)
				    ELSE NULL
				 END "Birth_Date_Time"
--				,TRY_PARSE(dbo.[legacy_Simple_Str2Date](a.[PID_7_1_Date_Time_of_Birth]) AS DATETIME) "Birth_Date_Time"
----,LEN(a.[PID_7_1_Date_Time_of_Birth]) "Len BD"
				,NULL "Body_Mass_Index"
				,b.C_Biosense_Facility_ID "c_Biosense_Facility_ID"
				,(FORMAT(CAST((dbo.Calc_Visit_Date_Time(
						a.[PV1_44_1_Admit_Date_Time],	  
						a.[PV1_45_1_Discharge_Date_Time], 
						a.[PR1_5_1_Procedure_Date_Time],  
						a.[PID_29_1_Patient_Death_Date_Time],
						a.[EVN_2_1_Recorded_Date_Time],		
						a.[MSH_7_1_Message_Date_Time]		
				  )) AS DATETIME), 'yyyy.MM.dd')
				 +'.'+LTRIM(RTRIM(ISNULL(CAST(b.C_Biosense_Facility_ID AS VARCHAR(50)),'<NullValue>')))
				 +'_'
				 +CASE
					WHEN a.[PID_3_1_Patient_ID_Internal] IS NOT NULL THEN a.[PID_3_1_Patient_ID_Internal]
					WHEN a.[PID_2_1_Patient_ID_External] IS NOT NULL THEN a.[PID_2_1_Patient_ID_External]
					WHEN a.[PID_4_1_Alternate_Patient_ID] IS NOT NULL THEN a.[PID_4_1_Alternate_Patient_ID]
					WHEN a.[PID_18_1_Patient_Account_ID] IS NOT NULL THEN a.[PID_18_1_Patient_Account_ID]
					WHEN a.[PV1_19_1_Patient_Visit_ID] IS NOT NULL THEN a.[PV1_19_1_Patient_Visit_ID]
					ELSE ''
				 END ) "c_BioSense_ID"
				,NULL "c_BMI"
				,CASE
					WHEN a.[OBX_5_1_Chief_Complaint] IS NOT NULL THEN a.[OBX_5_1_Chief_Complaint]
					WHEN a.[PV2_3_2_Admit_Reason_Text] IS NOT NULL THEN a.[PV2_3_2_Admit_Reason_Text] 
					WHEN a.[PV2_3_5_Admit_Reason_Alt_Text] IS NOT NULL THEN a.[PV2_3_5_Admit_Reason_Alt_Text]
					ELSE NULL
				 END "c_Chief_Complaint"
				,CASE
					WHEN a.[OBX_5_1_Chief_Complaint] IS NOT NULL THEN 'Chief_Complaint_Text'
					WHEN a.[PV2_3_2_Admit_Reason_Text] IS NOT NULL THEN 'Admit_Reason_Description' 
					WHEN a.[PV2_3_5_Admit_Reason_Alt_Text] IS NOT NULL THEN 'Admit_Reason_Description'
					ELSE NULL
				 END "c_Chief_Complaint_Source"
				,CASE
					WHEN LEFT(LTRIM(a.[PID_30_1_Patient_Death_Indicator] ),1)='Y' THEN 'Yes'
					WHEN a.[PID_29_1_Patient_Death_Date_Time] IS NOT NULL THEN 'Yes'
					WHEN CHARINDEX('20',a.[PV1_36_Discharge_Disposition])>0 THEN 'Yes'
					WHEN CHARINDEX('40',a.[PV1_36_Discharge_Disposition])>0 THEN 'Yes'
					WHEN CHARINDEX('41',a.[PV1_36_Discharge_Disposition])>0 THEN 'Yes'
					WHEN CHARINDEX('42',a.[PV1_36_Discharge_Disposition])>0 THEN 'Yes'
					ELSE 'No'
				 END "c_Death"
				,CASE
					WHEN LEFT(LTRIM(a.[PID_30_1_Patient_Death_Indicator] ),1)='Y' THEN 'Indicator'
					WHEN a.[PID_29_1_Patient_Death_Date_Time] IS NOT NULL THEN 'Death_Date'
					WHEN CHARINDEX('20',a.[PV1_36_Discharge_Disposition])>0 THEN 'Disposition'
					WHEN CHARINDEX('40',a.[PV1_36_Discharge_Disposition])>0 THEN 'Disposition'
					WHEN CHARINDEX('41',a.[PV1_36_Discharge_Disposition])>0 THEN 'Disposition'
					WHEN CHARINDEX('42',a.[PV1_36_Discharge_Disposition])>0 THEN 'Disposition'
					ELSE NULL
				 END "c_DeathSource"
-- Bob W. changed for PHINMS				,@Str_Site_ID+b.Output_FacilityID_UUID "c_Facility_ID"
				,CAST(b.Site_ID AS VARCHAR(20))+b.Output_FacilityID_UUID "c_Facility_ID"
				,'EVN-7.2' "c_Facility_ID_Source" 
--	Due to the value for "Facility_Type_Code" not be listed as mapped to the Processed table, this field should NULL as well.  If that changes
--		Then the field should be addressed the calculated variable.
--				,dbo.[legacy_c_FacTypePatientClass](d.[Facility_Type_Code]) "c_FacType_Patient_Class"
				,NULL "c_FacType_Patient_Class"
				,b.Patient_Class_Code "c_MFTPatientClass"
				,NULL "c_PatientAge"
				,NULL "c_PatientAgeSource"
				,NULL "c_PatientAgeUnits"
				,NULL "c_PatientAgeYears"
---- ** Due to Facility_Type_Code issue, other options will be commented out
				,CASE
					WHEN a.[PV1_2_1_Patient_Class] IS NOT NULL THEN LEFT(a.[PV1_2_1_Patient_Class],3)
					----WHEN d.[Facility_Type_Code] = '261QE0002X' THEN 'E'
					----WHEN d.[Facility_Type_Code] = '1021-5'     THEN 'I'
					----WHEN d.[Facility_Type_Code] = '261QM2500X' THEN 'O'
					----WHEN d.[Facility_Type_Code] = '261QP2300X' THEN 'O'
					----WHEN d.[Facility_Type_Code] = '261QU0200X' THEN 'O'
					WHEN b.Patient_Class_Code IS NOT NULL THEN LEFT(b.Patient_Class_Code,3)
					ELSE NULL
				 END "C_Patient_Class"
				,CASE
					WHEN [PV1_2_1_Patient_Class] IS NOT NULL	THEN 'PV1'
					----WHEN d.[Facility_Type_Code] = '261QE0002X'	THEN 'OBX'
					----WHEN d.[Facility_Type_Code] = '1021-5'		THEN 'OBX'
					----WHEN d.[Facility_Type_Code] = '261QM2500X'	THEN 'OBX'
					----WHEN d.[Facility_Type_Code] = '261QP2300X'	THEN 'OBX'
					----WHEN d.[Facility_Type_Code] = '261QU0200X'	THEN 'OBX'
					WHEN b.Patient_Class_Code IS NOT NULL		THEN 'MFT'
				 END  "C_Patient_Class_Source"
				,a.[PID_First_Patient_County] "C_Patient_County"
				,CASE WHEN a.[PID_First_Patient_County] IS NOT NULL THEN 'PID-12.1'
					ELSE Null
				 END "C_Patient_County_Source"
				,NULL "C_Processed_BioSense_ID"
				,NULL "C_Processed_Facility_ID"
				,CASE
					WHEN a.[PID_3_1_Patient_ID_Internal] IS NOT NULL THEN a.[PID_3_1_Patient_ID_Internal]
					WHEN a.[PID_2_1_Patient_ID_External] IS NOT NULL THEN a.[PID_2_1_Patient_ID_External]
					WHEN a.[PID_4_1_Alternate_Patient_ID] IS NOT NULL THEN a.[PID_4_1_Alternate_Patient_ID]
					WHEN a.[PID_18_1_Patient_Account_ID] IS NOT NULL THEN a.[PID_18_1_Patient_Account_ID]
					WHEN a.[PV1_19_1_Patient_Visit_ID] IS NOT NULL THEN a.[PV1_19_1_Patient_Visit_ID]
					ELSE NULL
				 END "C_Unique_Patient_ID"
				,CASE
					WHEN a.[PID_3_1_Patient_ID_Internal] IS NOT NULL THEN 'Legacy PID-3-1'
					WHEN a.[PID_2_1_Patient_ID_External] IS NOT NULL THEN 'Legacy PID-2-1'
					WHEN a.[PID_4_1_Alternate_Patient_ID] IS NOT NULL THEN 'Legacy PID-4'
					WHEN a.[PID_18_1_Patient_Account_ID] IS NOT NULL THEN 'Legacy PID-18-1'
					WHEN a.[PV1_19_1_Patient_Visit_ID] IS NOT NULL THEN 'Legacy PV1-19'
					ELSE NULL
				 END "C_Unique_Patient_ID_Source"
				,TRY_PARSE(dbo.Calc_Visit_Date_Time(
						a.[PV1_44_1_Admit_Date_Time],	  --@Admin
						a.[PV1_45_1_Discharge_Date_Time], --@Discharge
						a.[PR1_5_1_Procedure_Date_Time],  --@Procedure
						a.[PID_29_1_Patient_Death_Date_Time],--@Patient,
						a.[EVN_2_1_Recorded_Date_Time],		--@Recorded,
						a.[MSH_7_1_Message_Date_Time]		--@Message
				  ) AS Datetime) "C_Visit_Date"
				,CASE LEFT(dbo.Calc_Visit_Date_Source(
						a.[PV1_44_1_Admit_Date_Time],	  --@Admin
						a.[PV1_45_1_Discharge_Date_Time], --@Discharge
						a.[PR1_5_1_Procedure_Date_Time],  --@Procedure
						a.[PID_29_1_Patient_Death_Date_Time],--@Patient,
						a.[EVN_2_1_Recorded_Date_Time],		--@Recorded,
						a.[MSH_7_1_Message_Date_Time]		--@Message
						),1)
					WHEN 'A' THEN 'Admit'
					WHEN 'D' THEN 'Discharge'
					WHEN 'R' THEN 'Procedure'
					WHEN 'P' THEN 'Death'
					WHEN 'O' THEN 'Recorded'
					WHEN 'M' THEN 'Message'
					ELSE NULL 
				 END "C_Visit_Date_Source"
				,TRY_PARSE(dbo.Calc_Visit_Date_Time(
						a.[PV1_44_1_Admit_Date_Time],	  --@Admin
						a.[PV1_45_1_Discharge_Date_Time], --@Discharge
						a.[PR1_5_1_Procedure_Date_Time],  --@Procedure
						a.[PID_29_1_Patient_Death_Date_Time],--@Patient,
						a.[EVN_2_1_Recorded_Date_Time],		--@Recorded,
						a.[MSH_7_1_Message_Date_Time]		--@Message
				  ) AS DateTime) "C_Visit_Date_Time"
				,a.[Channel_Name] "Channel_Name"
				,NULL "Chief_Complaint_Code"
				,NULL "Chief_Complaint_Combo"
				,NULL "Chief_Complaint_Segment"
				,REPLACE(a.[OBX_5_1_Chief_Complaint],':SEP:',';')  "Chief_Complaint_Text"
				,NULL "Chief_Complaint_Type"
				,REPLACE(a.[OBX_5_1_Diagnosis_Impression],':SEP:',';')  "Clinical_Impression"
				,FORMAT(GETDATE() , 'MM/dd/yyyy HH:mm:ss') "Create_Processed_Date_Time"
				,FORMAT(a.[Create_Date_Time] , 'MM/dd/yyyy HH:mm:ss') "Create_Raw_Date_Time"
------
				,CASE 
					WHEN TRY_PARSE(a.[PID_29_1_Patient_Death_Date_Time] AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[PID_29_1_Patient_Death_Date_Time] AS DateTime2)
					WHEN [dbo].[Parse_HL7_Date_to_Datetime](a.[PID_29_1_Patient_Death_Date_Time],'I') IS NOT NULL THEN [dbo].[Parse_HL7_Date_to_Datetime](a.[PID_29_1_Patient_Death_Date_Time],'I') 	
					WHEN TRY_PARSE(a.[PID_29_1_Patient_Death_Date_Time]+':00' AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[PID_29_1_Patient_Death_Date_Time]+':00' AS DateTime2)
					WHEN LEN(a.[PID_29_1_Patient_Death_Date_Time])=6 THEN TRY_PARSE(a.[PID_29_1_Patient_Death_Date_Time]+'01' as datetime2)
					ELSE NULL
				 END "Death_Date_Time"
--				,TRY_PARSE((dbo.[legacy_Simple_Str2Date](a.[PID_29_1_Patient_Death_Date_Time])) AS DATETIME) "Death_Date_Time"
--				--,dbo.[legacy_Simple_Str2Date](a.[PID_29_1_Patient_Death_Date_Time]) "Death_Date_Timea"
--				--,a.[PID_29_1_Patient_Death_Date_Time] "Death_Date_Timeb"
------
				,REPLACE(a.[DG1_3_1_Diagnosis_Code],':',';')  "Diagnosis_Code"
				,NULL "Diagnosis_Combo"
				, CASE 
					WHEN TRY_PARSE(a.[DG1_5_1_Diagnosis_Date_Time] AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[DG1_5_1_Diagnosis_Date_Time] AS DateTime2)
					WHEN [dbo].[Parse_HL7_Date_to_Datetime](a.[DG1_5_1_Diagnosis_Date_Time],'I') IS NOT NULL THEN [dbo].[Parse_HL7_Date_to_Datetime](a.[DG1_5_1_Diagnosis_Date_Time],'I') 	
					WHEN TRY_PARSE(a.[DG1_5_1_Diagnosis_Date_Time]+':00' AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[DG1_5_1_Diagnosis_Date_Time]+':00' AS DateTime2)
					WHEN LEN(a.[DG1_5_1_Diagnosis_Date_Time])=6 THEN TRY_PARSE(a.[DG1_5_1_Diagnosis_Date_Time]+'01' as datetime2)
					ELSE NULL
				  END "Diagnosis_Date_Time"
--				,TRY_PARSE((dbo.[legacy_Simple_Str2Date](a.[DG1_5_1_Diagnosis_Date_Time])) AS DATETIME) "Diagnosis_Date_Time"
				,CASE 
					WHEN LEN(RTRIM(a.[DG1_3_2_Diagnosis_Text])) > 0 THEN REPLACE(a.[DG1_3_2_Diagnosis_Text],':',';')  
					WHEN LEN(RTRIM(a.[DG1_3_5_Diagnosis_Alt_Text])) > 0 THEN REPLACE(a.[DG1_3_5_Diagnosis_Alt_Text],':',';') 
					ELSE a.[DG1_3_5_Diagnosis_Alt_Text] 
				 END "Diagnosis_Description"
				,REPLACE(a.[DG1_15_1_Diagnosis_Priority],':',';') "Diagnosis_Priority"
				,NULL "Diagnosis_Segment"
				,REPLACE(a.[DG1_6_1_Diagnosis_Type],':',';') "Diagnosis_Type"
				,NULL "Diastolic_Blood_Pressure"
				,NULL "Diastolic_Blood_Pressure_Units"
				,CASE 
					WHEN TRY_PARSE(a.[PV1_45_1_Discharge_Date_Time] AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[PV1_45_1_Discharge_Date_Time] AS DateTime2)
					WHEN [dbo].[Parse_HL7_Date_to_Datetime](a.[PV1_45_1_Discharge_Date_Time],'I') IS NOT NULL THEN [dbo].[Parse_HL7_Date_to_Datetime](a.[PV1_45_1_Discharge_Date_Time],'I') 	
					WHEN TRY_PARSE(a.[PV1_45_1_Discharge_Date_Time]+':00' AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[PV1_45_1_Discharge_Date_Time]+':00' AS DateTime2)
					WHEN LEN(a.[PV1_45_1_Discharge_Date_Time])=6 THEN TRY_PARSE(a.[PV1_45_1_Discharge_Date_Time]+'01' as datetime2)
					ELSE NULL
				 END "Discharge_Date_Time"
--				,TRY_PARSE(([dbo].[legacy_Simple_Str2Date](a.[PV1_45_1_Discharge_Date_Time])) AS DATETIME) "Discharge_Date_Time"
				,REPLACE(a.[PV1_36_Discharge_Disposition],':',';') "Discharge_Disposition"
				,REPLACE(a.[OBX_5_1_Hospital_Discharge_Instr],':',';') "Discharge_Instructions"
				,REPLACE(a.[PID_22_Patient_Ethnic_Group],':',';') "Ethnicity_Code"
				,NULL "Ethnicity_Description"
				,NULL "Ethnicity_Segment"
				,a.[EVN_1_1_Event_Type_Code] "Event_Type_Code"
-- Following Line commented out due to Facility_Type_Code not being imported
--				,d.[Facility_Type_Code] "Facility_Type_Code"
				,NULL  "Facility_Type_Code"
				,NULL "Facility_Type_Description"
				,NULL "Facility_Type_Segment"
				,COALESCE(a.[Feed_Name],'Legacy Unknown') "Feed_Name"
				,REPLACE(a.[PID_3_1_Patient_ID_Internal],':',';') "First_Patient_ID"
				,NULL "First_Patient_ID_Assigning_Authority"
				,NULL "First_Patient_ID_Assigning_Facility"
				,NULL "First_Patient_ID_Type_Code"
				,NULL "Height"
				,NULL "Height_Units"
				,REPLACE(a.[PV1_10_1_Hospital_Service],':',';') "Hospital_Service"
				,NULL "Hospital_Unit_Code"
				,NULL "Hospital_Unit_Description"
				,NULL "Hospital_Unit_Segment"
				,REPLACE(a.[OBX_5_1_Acuity_Assessment],':',';') "Initial_Acuity_Code"
				,NULL "Initial_Acuity_Combo"
				,NULL "Initial_Acuity_Description"
				,NULL "Initial_Acuity_Segment"
				,REPLACE(a.[OBX_5_1_Initial_Evaluation_Note],':',';') "Initial_Evaluation_Note"
				,CAST(TRY_PARSE((dbo.Get_First_Value(REPLACE(a.[OBX_5_1_Initial_Pulse],':',';'),';')) AS float) AS INT) "Initial_Pulse_Oximetry"
				,REPLACE(a.[OBX_6_2_Initial_Pulse_Units],':',';') "Initial_Pulse_Oximetry_Units"
				,TRY_PARSE(a.[OBX_5_1_Body_Temperature] as DECIMAL(6,2)) "Initial_Temp"
				,REPLACE(a.[OBX_6_2_Body_Temperature_Units],':',';') "Initial_Temp_Units"
				,NULL "Insurance_Company_ID"
				,NULL "Insurance_Coverage"
				,'Y' "Legacy_Flag"
				,a.[Row_Number] "Legacy_Row_Number"
				,NULL "Medical_Record_Number"
				,NULL "Medical_Record_Number_Assigning_Authority"
				,NULL "Medical_Record_Number_Assigning_Facility"
				,NULL "Medication_Code"
				,NULL "Medication_Combo"
				,NULL "Medication_Description"
				,REPLACE(a.[OBX_5_1_Medication_History],':',';') "Medication_List"
				,NULL "Medication_Segment"
				,a.[MSH_10_1_Message_Control_ID] "Message_Control_ID"
				,CASE 
					WHEN TRY_PARSE(a.[MSH_7_1_Message_Date_Time] AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[MSH_7_1_Message_Date_Time] AS DateTime2)
					WHEN [dbo].[Parse_HL7_Date_to_Datetime](a.[MSH_7_1_Message_Date_Time],'I') IS NOT NULL THEN [dbo].[Parse_HL7_Date_to_Datetime](a.[MSH_7_1_Message_Date_Time],'I') 	
					WHEN TRY_PARSE(a.[MSH_7_1_Message_Date_Time]+':00' AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[MSH_7_1_Message_Date_Time]+':00' AS DateTime2)
					WHEN LEN(a.[MSH_7_1_Message_Date_Time])=6 THEN TRY_PARSE(a.[MSH_7_1_Message_Date_Time]+'01' as datetime2)
				    ELSE NULL
				 END "Message_Date_Time"
--				,COALESCE(TRY_PARSE((dbo.[legacy_Simple_Str2Date](a.[MSH_7_1_Message_Date_Time])) AS DATETIME),a.[Create_Date_Time]) "Message_Date_Time"
				,(NEXT VALUE FOR [Global_Message_ID]) "Message_ID" -- (NEXT VALUE FOR a.[Global_Message_ID])
				,a.[MSH_21_1_Message_Profile_ID] "Message_Profile_ID"
				,a.[MSH_9_3_Message_Structure] "Message_Structure"
				,ISNULL(a.[MSH_9_1_Message_Code],'ADT') "Message_Type"
				,CASE 
					WHEN TRY_PARSE(a.[OBX_14_1_Observation_Date_Time] AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[OBX_14_1_Observation_Date_Time] AS DateTime2)
					WHEN [dbo].[Parse_HL7_Date_to_Datetime](a.[OBX_14_1_Observation_Date_Time],'I') IS NOT NULL THEN [dbo].[Parse_HL7_Date_to_Datetime](a.[OBX_14_1_Observation_Date_Time],'I') 	
					WHEN TRY_PARSE(a.[OBX_14_1_Observation_Date_Time]+':00' AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[OBX_14_1_Observation_Date_Time]+':00' AS DateTime2)
					WHEN LEN(a.[OBX_14_1_Observation_Date_Time])=6 THEN TRY_PARSE(a.[OBX_14_1_Observation_Date_Time]+'01' as datetime2)
					ELSE NULL
				 END "Observation_Date_Time"
--				,TRY_PARSE(([dbo].[legacy_Simple_Str2Date](a.[OBX_14_1_Observation_Date_Time])) AS DATETIME) "Observation_Date_Time"
				,CASE 
					WHEN TRY_PARSE(a.[OBX_5_1_Onset_Date_Time] AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[OBX_5_1_Onset_Date_Time] AS DateTime2)
					WHEN [dbo].[Parse_HL7_Date_to_Datetime](a.[OBX_5_1_Onset_Date_Time],'I') IS NOT NULL THEN [dbo].[Parse_HL7_Date_to_Datetime](a.[OBX_5_1_Onset_Date_Time],'I') 	
					WHEN TRY_PARSE(a.[OBX_5_1_Onset_Date_Time]+':00' AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[OBX_5_1_Onset_Date_Time]+':00' AS DateTime2)
					WHEN LEN(a.[OBX_5_1_Onset_Date_Time])=6 THEN TRY_PARSE(a.[OBX_5_1_Onset_Date_Time]+'01' as datetime2)
					ELSE NULL
				 END "Onset_Date"
--				,TRY_PARSE(([dbo].[legacy_Simple_Str2Date](a.[OBX_5_1_Onset_Date_Time])) AS DATETIME) "Onset_Date"
				,a.[PID_18_1_Patient_Account_ID] "Patient_Account_Number"
				,a.[PID_11_3_Patient_City] "Patient_City"
				,a.[PV1_2_1_Patient_Class] "Patient_Class_Code"
				,a.[PID_11_6_Patient_Country] "Patient_Country"
				,a.[PID_30_1_Patient_Death_Indicator] "Patient_Death_Indicator"
				,a.[PID_11_4_Patient_State] "Patient_State"
				,a.[PID_11_5_Patient_Zip] "Patient_Zip"
				,a.[OBX_5_1_Pregnancy_Status] "Pregnancy_Status_Code"
				,NULL "Pregnancy_Status_Description"
				,NULL "Pregnancy_Status_Segment"
				,NULL "Previous_Hospital_Unit"
				,REPLACE(a.[OBX_5_1_Problem_or_Finding],':',';') "Problem_List_Code"
				,NULL "Problem_List_Combo"
				,NULL "Problem_List_Description"
				,NULL "Problem_List_Segment"
				,REPLACE(a.[PR1_3_1_Procedure_Code_ID],':',';') "Procedure_Code"
				,NULL "Procedure_Combo"
				,CASE 
					WHEN TRY_PARSE(a.[PR1_5_1_Procedure_Date_Time] AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[PR1_5_1_Procedure_Date_Time] AS DateTime2)
					WHEN [dbo].[Parse_HL7_Date_to_Datetime](a.[PR1_5_1_Procedure_Date_Time],'I') IS NOT NULL THEN [dbo].[Parse_HL7_Date_to_Datetime](a.[PR1_5_1_Procedure_Date_Time],'I') 	
					WHEN TRY_PARSE(a.[PR1_5_1_Procedure_Date_Time]+':00' AS DateTime2) IS NOT NULL THEN TRY_PARSE(a.[PR1_5_1_Procedure_Date_Time]+':00' AS DateTime2)
					WHEN LEN(a.[PR1_5_1_Procedure_Date_Time])=6 THEN TRY_PARSE(a.[PR1_5_1_Procedure_Date_Time]+'01' as datetime2)
					ELSE NULL
				 END "Procedure_Date_Time"
--				,TRY_PARSE(([dbo].[legacy_Simple_Str2Date](a.[PR1_5_1_Procedure_Date_Time])) AS DATETIME) "Procedure_Date_Time"
				,REPLACE(a.[PR1_3_2_Procedure_Code_Text],':',';') "Procedure_Description"
				,NULL "Procedure_Segment"
				--,NULL "Processed_ID"
				,a.[MSH_11_1_Processing_ID] "Processing_ID"
				,NULL "Provider_Type_Code"
				,NULL "Provider_Type_Combo"
				,NULL "Provider_Type_Description"
				,NULL "Provider_Type_Segment"
				,REPLACE(a.[PID_10_Patient_Race],':',';') "Race_Code"
				,NULL "Race_Description"
				,NULL "Race_Segment"
				,a.[MSH_5_1_Receiving_Application] "Receiving_Application"
				,a.[MSH_6_1_Receiving_Facility] "Receiving_Facility"
				,CASE 
					WHEN TRY_PARSE([EVN_2_1_Recorded_Date_Time] AS DateTime2) IS NOT NULL THEN TRY_PARSE([EVN_2_1_Recorded_Date_Time] AS DateTime2)
					WHEN [dbo].[Parse_HL7_Date_to_Datetime]([EVN_2_1_Recorded_Date_Time],'I') IS NOT NULL THEN [dbo].[Parse_HL7_Date_to_Datetime]([EVN_2_1_Recorded_Date_Time],'I') 	
					WHEN TRY_PARSE([EVN_2_1_Recorded_Date_Time]+':00' AS DateTime2) IS NOT NULL THEN TRY_PARSE([EVN_2_1_Recorded_Date_Time]+':00' AS DateTime2)
					WHEN LEN(a.[EVN_2_1_Recorded_Date_Time])=6 THEN TRY_PARSE(a.[EVN_2_1_Recorded_Date_Time]+'01' as datetime2)
					ELSE NULL
				 END "Recorded_Date_Time"
--				,TRY_PARSE(([dbo].[legacy_Simple_Str2Date](a.[EVN_2_1_Recorded_Date_Time])) AS DATETIME) "Recorded_Date_Time"
				,a.[MSH_3_1_Sending_Application] "Sending_Application"
				,a.[MSH_4_Sending_Facility] "Sending_Facility_ID"
				,NULL "Sending_Facility_ID_Source"
				,a.[PV1_39_1_Servicing_Facility] "Servicing_Facility"
-- Bob W. need retrieved Site				,@Site_ID "Site_ID" -- Based on a.[Feed_Name]
				,b.Site_ID "Site_ID" -- Based on a.[Feed_Name]
				,NULL "Smoking_Status_Code"
				,NULL "Smoking_Status_Description"
				,NULL "Smoking_Status_Segment"
				,a.[PV1_44_1_Admit_Date_Time] "Str_Admit_Date_Time"
				,REPLACE(a.[OBX_5_1_Patient_Age_Calculated],':',';') "Str_Age_Calculated"
				,REPLACE(a.[OBX_5_1_Patient_Age_Reported],':',';')  "Str_Age_Reported"
				,FORMAT(a.Create_Date_Time , 'MM/dd/yyyy HH:mm:ss') "Str_Arrived_Date_Time"
				,a.[PID_7_1_Date_Time_of_Birth] "Str_Birth_Date_Time"
				,NULL "Str_Body_Mass_Index"
				,a.[PID_29_1_Patient_Death_Date_Time]  "Str_Death_Date_Time"
				,a.[DG1_5_1_Diagnosis_Date_Time] "Str_Diagnosis_Date_Time"
				,NULL "Str_Diastolic_Blood_Pressure"
				,PV1_45_1_Discharge_Date_Time "Str_Discharge_Date_Time"
				,NULL "Str_Height"
				,a.[OBX_5_1_Initial_Pulse] "Str_Initial_Pulse_Oximetry"
				,a.[OBX_5_1_Body_Temperature] "Str_Initial_Temp"
				,a.[MSH_7_1_Message_Date_Time] "Str_Message_Date_Time"
				,a.[OBX_14_1_Observation_Date_Time] "Str_Observation_Date_Time"
				,a.[OBX_5_1_Onset_Date_Time] "Str_Onset_Date"
				,a.[PR1_5_1_Procedure_Date_Time] "Str_Procedure_Date_Time"
				,a.[EVN_2_1_Recorded_Date_Time] "Str_Recorded_Date_Time"
				,NULL "Str_Systolic_Blood_Pressure"
--need to map raw blood pressure value into string field				
--				,NULL "Str_Systolic_Diastolic_Blood_Pressure"
				,a.[OBX_5_2_Blood_Pressure] "Str_Systolic_Diastolic_Blood_Pressure"
				,NULL "Str_Weight"
				,NULL "Systolic_Blood_Pressure"
				,NULL "Systolic_Blood_Pressure_Units"
--"Systolic_Diastolic_Blood_Pressure" is an int column so this will cause an error				
--				,REPLACE(a.[OBX_5_2_Blood_Pressure],':',';') "Systolic_Diastolic_Blood_Pressure"
				,NULL "Systolic_Diastolic_Blood_Pressure"
				,REPLACE(a.[OBX_6_2_Blood_Pressure_Units],':',';') "Systolic_Diastolic_Blood_Pressure_Units"
				--,CASE
				--	WHEN CHARINDEX('+',a.[MSH_7_1_Message_Date_Time])>0 THEN (LEFT(a.[MSH_7_1_Message_Date_Time],CHARINDEX('+',a.[MSH_7_1_Message_Date_Time]-1)))
				--	WHEN CHARINDEX('-',a.[MSH_7_1_Message_Date_Time])>0 THEN (LEFT(a.[MSH_7_1_Message_Date_Time],CHARINDEX('-',a.[MSH_7_1_Message_Date_Time]-1)))
				--	ELSE NULL
				-- END "Time_Zone"
				,NULL "Time_Zone"
				,NULL "Travel_History"
				,a.[EVN_7_2_Event_Facility] "Treating_Facility_ID"
				,REPLACE(a.[OBX_5_1_Triage_Notes],':SEP:',';') "Triage_Notes"
				,a.[MSH_9_2_Trigger_Event] "Trigger_Event"
				,NULL "Unique_Physician_Identifier"
				,NULL "Unique_Physician_Identifier_Assigning_Authority"
				,'L' "Update_Essence"
				,FORMAT(GETDATE() , 'MM/dd/yyyy HH:mm:ss') "Update_Processed_Date_Time"
				,a.[MSH_12_1_Version_ID] "Version_ID"
				,a.[PV1_19_1_Patient_Visit_ID] "Visit_ID"
				,a.[PV1_19_4_Assigning_Authority] "Visit_ID_Assigning_Authority"
				,a.[PV1_19_6_Assigning_Facility] "Visit_ID_Assigning_Facility"
				,NULL "Weight"
				,NULL "Weight_Units"
-- Temp Values from OCW
				,b.[Output_FacilityID_UUID]
				,b.[Input_FacilityID_UUID]
				,a.[EVN_7_2_Event_Facility]
				,a.[MSH_4_Sending_Facility]
				,b.[Site_ID]
				,CASE
					WHEN b.[Input_FacilityID_UUID] IS NOT NULL THEN 1
					ELSE 0
				 END "EVN_MSH_Resolved"

			FROM [Legacy_BioSense].[dbo].[PH_Stage_1_Archive] a(NOLOCK) 
				JOIN @Control_Table t ON a.[Row_Number] = t.[Row_Number]  -- This used to reduce the dataset for testing, it should be removed for full processing
--  Bob W., we need information for all sites	
--				--LEFT JOIN [Master_Profile].[dbo].[Operational_Crosswalk] (NOLOCK) b ON a.[EVN_7_2_Event_Facility] = b.[Input_FacilityID_UUID] AND @Site_ID = b.Site_ID
				LEFT JOIN [Master_Profile].[dbo].[Operational_Crosswalk] (NOLOCK) b ON a.[EVN_7_2_Event_Facility] = b.[Input_FacilityID_UUID] 
--				LEFT JOIN [Master_Profile].[dbo].[Operational_Crosswalk] (NOLOCK) b ON a.[EVN_7_2_Event_Facility] = b.[Input_FacilityID_UUID]	AND @Site_ID = b.Site_ID and b.Facility_Status in ('Active','Inactive' )
				--WHERE ( (a.[Row_Number] between @StartID and @EndID) 
				--	AND (a.Create_Date_Time >= @Begin_Date AND Create_Date_Time < @End_Date)
				--)

			IF (@Debug = 1)
				PRINT 'Mark #1'

			--MSH_4_Sending_Facility (Sending_Facility_ID) is present and can be mapped with a valid status � No Exception
			UPDATE #TempTable SET 
					 C_Biosense_Facility_ID = b.C_Biosense_Facility_ID
					,C_Biosense_ID = REPLACE(a.[C_Biosense_ID],'<NullValue>',LTRIM(RTRIM(STR(b.[C_Biosense_Facility_ID]))))
					,C_Facility_ID = CAST(b.Site_ID as varchar(20))+b.[Output_FacilityID_UUID]
					,C_Facility_ID_Source = 'MSH-4.2'
					,C_MFT_Patient_Class  = b.[Patient_Class_Code]
					,C_Patient_Class        = CASE
									WHEN a.[Patient_Class_Code] IS NOT NULL THEN LEFT(a.[Patient_Class_Code],3)
									WHEN b.[Patient_Class_Code] IS NOT NULL THEN LEFT(b.[Patient_Class_Code],3)
									ELSE NULL
								 END
					,C_Patient_Class_Source = CASE
									WHEN a.[Patient_Class_Code] IS NOT NULL	THEN 'PV1'
									WHEN b.[Patient_Class_Code] IS NOT NULL		THEN 'MFT'
									ELSE NULL
								 END
					,Diagnosis_Code = LEFT(';'+RTRIM(Diagnosis_Code),255)
					,Diagnosis_Description = LEFT(';'+RTRIM(Diagnosis_Description),3000)
					,EVN_MSH_Resolved = 
					  CASE
						WHEN b.[Input_FacilityID_UUID] IS NOT NULL THEN 1
						ELSE 0
					   END 
					,Input_FacilityID_UUID = b.[Input_FacilityID_UUID]
					,Site_ID=b.Site_ID

			FROM #TempTable a
				JOIN [Master_Profile].[dbo].[Operational_Crosswalk] (NOLOCK) b
-- Bob W. not by Site in PHINMS				      ON a.[MSH_4_Sending_Facility] = b.[Input_FacilityID_UUID] AND @Site_ID = b.Site_ID
				      ON a.[MSH_4_Sending_Facility] = b.[Input_FacilityID_UUID] 
			WHERE  ( (a.Input_FacilityID_UUID IS NULL) AND (EVN_MSH_Resolved = 0) )

			IF (@Debug = 1)
				PRINT 'Mark #2'

	--EVN_7_2_Event_Facility (Treating_Facility_ID) is present but cannot be mapped � Exception Code 04 
			UPDATE #TempTable SET
					 C_Biosense_Facility_ID = NULL
					,C_Biosense_ID = REPLACE(a.[C_Biosense_ID],'<NullValue>','')
					,C_Facility_ID = CAST(a.[Hold_Site_ID] AS VARCHAR(20))+a.[EVN_7_2_Event_Facility]
					,C_Facility_ID_Source = 'EVN-7.2'
					,C_MFT_Patient_Class  = NULL
					,C_Patient_Class = CASE
									WHEN a.[Patient_Class_Code] IS NOT NULL THEN LEFT(a.[Patient_Class_Code],3)
									ELSE NULL
								 END
					,C_Patient_Class_Source = CASE
									WHEN a.[Patient_Class_Code] IS NOT NULL	THEN 'PV1'
									ELSE NULL
								 END
					,EVN_MSH_Resolved = 
					  CASE
						WHEN a.[EVN_7_2_Event_Facility] IS NOT NULL THEN 1
						ELSE 0
					   END 
					,Site_ID = a.[Hold_Site_ID]
					,Input_FacilityID_UUID = a.[EVN_7_2_Event_Facility]
			FROM #TempTable a
			WHERE ( (a.Input_FacilityID_UUID IS NULL) AND (EVN_MSH_Resolved = 0) )

			IF (@Debug = 1)
				PRINT 'Mark #3'

	--MSH_4_Sending_Facility (Sending_Facility_ID) is present but cannot be mapped � Exception Code 04
			UPDATE #TempTable SET
					 C_Biosense_Facility_ID = NULL
					,C_Biosense_ID = REPLACE(a.[C_Biosense_ID],'<NullValue>','')
					,C_Facility_ID = CAST(a.Site_ID AS VARCHAR(20))+a.[MSH_4_Sending_Facility]
					,C_Facility_ID_Source = 'MSH-4.2'
					,C_MFT_Patient_Class  = NULL
					,C_Patient_Class = CASE
									WHEN a.[Patient_Class_Code] IS NOT NULL THEN LEFT(a.[Patient_Class_Code],3)
									ELSE NULL
								 END
					,C_Patient_Class_Source = CASE
									WHEN a.[Patient_Class_Code] IS NOT NULL	THEN 'PV1'
									ELSE NULL
								 END
					,EVN_MSH_Resolved = 
					  CASE
						WHEN a.[MSH_4_Sending_Facility] IS NOT NULL THEN 1
						ELSE 0
					   END 
					,Input_FacilityID_UUID = a.[MSH_4_Sending_Facility]
			FROM #TempTable a
			WHERE ( (a.Input_FacilityID_UUID IS NULL) AND (EVN_MSH_Resolved = 0) )

			IF (@Debug = 1)
				PRINT 'Mark #4'

			UPDATE [dbo].[#TempTable] 
			SET  C_Processed_BioSense_ID = Convert(varchar(20), C_Visit_Date , 102)+'.'+ LTRIM(RTRIM(STR(C_BioSense_Facility_ID)))+C_Patient_Class+'_'+C_Unique_Patient_ID
				,C_Processed_Facility_ID = C_Facility_ID+C_Patient_Class
				,C_Patient_Age           = [dbo].[Calc_Patient_Age](C_Visit_Date_Time,Birth_Date_Time,Age_Reported,Age_Units_Reported,Age_Calculated,Age_Units_Calculated,'Age')
				,C_Patient_Age_Units     = [dbo].[Calc_Patient_Age](C_Visit_Date_Time,Birth_Date_Time,Age_Reported,Age_Units_Reported,Age_Calculated,Age_Units_Calculated,'AgeUnits')
				,C_Patient_Age_Source    = [dbo].[Calc_Patient_Age](C_Visit_Date_Time,Birth_Date_Time,Age_Reported,Age_Units_Reported,Age_Calculated,Age_Units_Calculated,'AgeSource')
				,C_Patient_Age_Years     = [dbo].[Calc_Patient_Age_Years]
					([dbo].[Calc_Patient_Age](C_Visit_Date_Time,Birth_Date_Time,Age_Reported,Age_Units_Reported,Age_Calculated,Age_Units_Calculated,'Age'),
					 [dbo].[Calc_Patient_Age](C_Visit_Date_Time,Birth_Date_Time,Age_Reported,Age_Units_Reported,Age_Calculated,Age_Units_Calculated,'AgeUnits')
					)
		
		
		
		-- Clear out exceptions
		DELETE @Exceptions_Count
		SET @Exception_Count = 0

	
		-- Record validation section

			-- Check for exceptions in with the record

			IF (@Debug = 1)
				PRINT 'Mark #5'

			-- Invalid C_Unique_Patient_ID
			INSERT INTO @Exceptions_Count (Message_ID, reason_code)
				SELECT Message_ID,'01'
				FROM #TempTable WHERE ( (Isnull(C_Unique_Patient_ID,'')='' or DATALENGTH(C_Unique_Patient_ID)<=2) )


			--/*Message did not include valid C_Visit_Date_Time*/
			INSERT INTO @Exceptions_Count (Message_ID, reason_code)
				SELECT Message_ID,'02'
				FROM #TempTable WHERE ( (Isnull(C_Visit_Date_Time,'')='') or (DATALENGTH(C_Visit_Date_Time)<6) )

			--If C_Facility_ID IS NULL
			INSERT INTO @Exceptions_Count (Message_ID, reason_code)
				SELECT Message_ID,'03'
				FROM #TempTable WHERE ( ISNULL(C_Facility_ID,'')='' )

			--If C_Facility_ID IS NULL and not found in OCW
			INSERT INTO @Exceptions_Count (Message_ID, reason_code)
				SELECT Message_ID,'04'
				FROM #TempTable a WHERE ( ( ISNULL(a.C_Facility_ID,'')<>'' ) AND (ISNULL(a.[C_Biosense_Facility_ID],'')='') )

			--If C_Facility_ID IS NULL and not an active or inactive facility
			INSERT INTO @Exceptions_Count (Message_ID, reason_code)
				SELECT Message_ID,'05'
				FROM #TempTable a 
				WHERE 
				( (( ISNULL(a.C_Facility_ID,'')<>'' ) AND (ISNULL(a.[C_Biosense_Facility_ID],'')<>'') )
						AND
				( (Select count(*) from Master_Profile.dbo.Operational_Crosswalk o where o.Site_id=a.Site_ID and (CAST(a.Site_ID AS VARCHAR(20))+o.Output_FacilityID_UUID=Ltrim(Rtrim(a.C_Facility_ID)))
--						and o.Facility_Status in ('Active','Inactive' )) = 0)
						AND (o.Facility_Status not in ('Active','InActive') ) ) = 1)--add Facility_Status values you do NOT want to exception out
				)
				
			--If Site_ID IS NULL
			INSERT INTO @Exceptions_Count (Message_ID, reason_code)
				SELECT Message_ID,'06'
				FROM #TempTable WHERE ( Site_ID IS NULL )

			--/*Message did not include valid Arrived_Date_Time*/
			INSERT INTO @Exceptions_Count (Message_ID, reason_code)
				SELECT Message_ID,'08'
				FROM #TempTable WHERE (Isnull(Arrived_Date_Time,'')='')

			--/*Message did not include valid Create_Raw_Date_Time*/
			INSERT INTO @Exceptions_Count (Message_ID, reason_code)
				SELECT Message_ID,'09'
				FROM #TempTable WHERE (Isnull(Create_Raw_Date_Time,'')='')

			-- Invalid Message_ID
			INSERT INTO @Exceptions_Count (Message_ID, reason_code)
				SELECT Message_ID,'10'
				FROM #TempTable WHERE ( Isnull(Message_ID,0)=0 )

			--/*C_Visit_Date_Time is a future date */
			INSERT INTO @Exceptions_Count (Message_ID, reason_code)
				SELECT Message_ID,'11'
				FROM #TempTable WHERE ( C_Visit_Date_Time > DATEADD(HOUR, 12, Arrived_Date_Time) )

			--/*MSH_4_Sending_Facility IS NULL */
			INSERT INTO @Exceptions_Count (Message_ID, reason_code)
				SELECT Message_ID,'13'
				FROM #TempTable WHERE ( ISNULL(MSH_4_Sending_Facility,'')='')

			------/*Meets site-provided criteria for exclusion*/	
			----INSERT INTO @Exceptions_Count (Message_ID, reason_code)
			----	SELECT Message_ID,'99'
			----	FROM #TempTable a
			----	JOIN [Legacy_Biosense].[dbo].[PH_Site_Exclusions] b on a.Input_FacilityID_UUID=b.FacilityID_UUIDIDPH
			----	AND ( ( b.Date_Check = 0) OR (( b.Date_Check = 1) AND (a.Admit_Date_Time between b.Min_PV1_44_1_Admit_Date_Time_Code AND b.Max_PV1_44_1_Admit_Date_Time_Code)) )
			----	AND a.Input_FacilityID_UUID IS NOT NULL
				
			IF (@Debug = 1)
				PRINT 'Mark #6'

	-- ** Need to process valiation checks prior to getting here
	-- Process the Exception Record

				SELECT @Exception_Count = COUNT(*) FROM @Exceptions_Count

				IF ( @Exception_Count > 0)
				  BEGIN
					INSERT [dbo].[PH_Staging_Exceptions] 
						(Message_ID, Str_Arrived_Date_Time, Arrived_Date_Time, Create_Raw_Date_Time, Feed_Name, Channel_Name, Sending_Application, Receiving_Application, Receiving_Facility, Message_Type, Trigger_Event, Message_Structure, Message_Control_ID, Processing_ID,Version_ID
								, Message_Profile_ID, Site_ID, Admit_Reason_Code, Admit_Reason_Description, Admit_Reason_Segment, Admit_Reason_Combo, Chief_Complaint_Code, Chief_Complaint_Text, Chief_Complaint_Segment, Chief_Complaint_Combo, Chief_Complaint_Type, C_Chief_Complaint
								, C_Chief_Complaint_Source, Str_Age_Calculated, Age_Calculated, Age_Units_Calculated, Str_Age_Reported, Age_Reported, Age_Units_Reported, Str_Birth_Date_Time, Birth_Date_Time, C_Patient_Age, C_Patient_Age_Units, C_Patient_Age_Years, C_Patient_Age_Source
								, Patient_Class_Code, Facility_Type_Code, Facility_Type_Description, Facility_Type_Segment, C_FacType_Patient_Class, C_Patient_Class, C_Patient_Class_Source, Treating_Facility_ID, Sending_Facility_ID, C_Facility_ID, Sending_Facility_ID_Source, C_Facility_ID_Source
								, C_Processed_Facility_ID, Str_Admit_Date_Time, Admit_Date_Time, Str_Discharge_Date_Time, Discharge_Date_Time, Str_Observation_Date_Time, Observation_Date_Time, Str_Procedure_Date_Time, Procedure_Date_Time, Str_Death_Date_Time, Death_Date_Time, Str_Recorded_Date_Time
								, Recorded_Date_Time, Str_Message_Date_Time, Message_Date_Time, Str_Diagnosis_Date_Time, Diagnosis_Date_Time, C_Visit_Date_Time, C_Visit_Date, C_Visit_Date_Source, First_Patient_ID, First_Patient_ID_Assigning_Authority, First_Patient_ID_Assigning_Facility
								, First_Patient_ID_Type_Code, Patient_Account_Number, Visit_ID, Visit_ID_Assigning_Authority, Visit_ID_Assigning_Facility, Medical_Record_Number, Medical_Record_Number_Assigning_Authority, Medical_Record_Number_Assigning_Facility, C_Unique_Patient_ID
								, C_Unique_Patient_ID_Source, Patient_Death_Indicator, C_Death, C_Death_Source, C_BioSense_ID, C_Processed_BioSense_ID, Patient_Zip, C_Patient_County, C_Patient_County_Source, Diagnosis_Code, Diagnosis_Description, Diagnosis_Segment, Diagnosis_Combo, Diagnosis_Type
								, Diagnosis_Priority, Discharge_Disposition, Triage_Notes, Administrative_Sex, Race_Code, Race_Description, Race_Segment, Ethnicity_Code, Ethnicity_Description, Ethnicity_Segment, Str_Initial_Temp, Initial_Temp, Initial_Temp_Units, Alternate_Visit_ID, Procedure_Code
								, Procedure_Description, Procedure_Segment, Procedure_Combo, Str_Onset_Date, Onset_Date, Clinical_Impression, Problem_List_Code, Problem_List_Description, Problem_List_Segment, Problem_List_Combo, Str_Initial_Pulse_Oximetry, Initial_Pulse_Oximetry
								, Initial_Pulse_Oximetry_Units, Initial_Acuity_Code, Initial_Acuity_Description, Initial_Acuity_Segment, Initial_Acuity_Combo, Servicing_Facility, Unique_Physician_Identifier, Unique_Physician_Identifier_Assigning_Authority, Provider_Type_Code, Provider_Type_Description
								, Provider_Type_Segment, Provider_Type_Combo, Patient_City, Patient_State, Patient_Country, Hospital_Unit_Code, Hospital_Unit_Description, Hospital_Unit_Segment, Str_Height, Height, Height_Units, Str_Weight, Weight, Weight_Units, Str_Body_Mass_Index, Body_Mass_Index
								, Smoking_Status_Code, Smoking_Status_Description, Smoking_Status_Segment, Str_Systolic_Blood_Pressure, Systolic_Blood_Pressure, Systolic_Blood_Pressure_Units, Str_Diastolic_Blood_Pressure, Diastolic_Blood_Pressure, Diastolic_Blood_Pressure_Units
								, Str_Systolic_Diastolic_Blood_Pressure, Systolic_Diastolic_Blood_Pressure, Systolic_Diastolic_Blood_Pressure_Units, Event_Type_Code, Insurance_Coverage, Insurance_Company_ID, Medication_List, Medication_Code, Medication_Description, Medication_Segment
								, Medication_Combo, Previous_Hospital_Unit, Pregnancy_Status_Code, Pregnancy_Status_Description, Pregnancy_Status_Segment, Hospital_Service, Admit_Source, Ambulatory_Status, Admission_Type, Travel_History, Discharge_Instructions, Initial_Evaluation_Note
								, C_MFT_Patient_Class, Time_Zone, Create_Processed_Date_Time, Update_Processed_Date_Time, Assigned_Patient_Location, Legacy_Flag, Legacy_Row_Number, C_Biosense_Facility_ID)  
								-- Left out Update_Essence, C_BMI
						SELECT 
							Message_ID, Str_Arrived_Date_Time, Arrived_Date_Time, Create_Raw_Date_Time, Feed_Name, Channel_Name, Sending_Application, Receiving_Application, Receiving_Facility, Message_Type, Trigger_Event, Message_Structure, Message_Control_ID, Processing_ID, Version_ID
							, Message_Profile_ID, Site_ID, Admit_Reason_Code, Admit_Reason_Description, Admit_Reason_Segment, Admit_Reason_Combo, Chief_Complaint_Code, Chief_Complaint_Text, Chief_Complaint_Segment, Chief_Complaint_Combo, Chief_Complaint_Type, C_Chief_Complaint
							, C_Chief_Complaint_Source, Str_Age_Calculated, Age_Calculated, Age_Units_Calculated, Str_Age_Reported, Age_Reported, Age_Units_Reported, Str_Birth_Date_Time, Birth_Date_Time, C_Patient_Age, C_Patient_Age_Units, C_Patient_Age_Years, C_Patient_Age_Source
							, Patient_Class_Code, Facility_Type_Code, Facility_Type_Description, Facility_Type_Segment, C_FacType_Patient_Class, C_Patient_Class, C_Patient_Class_Source, Treating_Facility_ID, Sending_Facility_ID, C_Facility_ID, Sending_Facility_ID_Source, C_Facility_ID_Source
							, C_Processed_Facility_ID, Str_Admit_Date_Time, Admit_Date_Time, Str_Discharge_Date_Time, Discharge_Date_Time, Str_Observation_Date_Time, Observation_Date_Time, Str_Procedure_Date_Time, Procedure_Date_Time, Str_Death_Date_Time, Death_Date_Time, Str_Recorded_Date_Time
							, Recorded_Date_Time, Str_Message_Date_Time, Message_Date_Time, Str_Diagnosis_Date_Time, Diagnosis_Date_Time, C_Visit_Date_Time, C_Visit_Date, C_Visit_Date_Source, First_Patient_ID, First_Patient_ID_Assigning_Authority, First_Patient_ID_Assigning_Facility
							, First_Patient_ID_Type_Code, Patient_Account_Number, Visit_ID, Visit_ID_Assigning_Authority, Visit_ID_Assigning_Facility, Medical_Record_Number, Medical_Record_Number_Assigning_Authority, Medical_Record_Number_Assigning_Facility, C_Unique_Patient_ID
							, C_Unique_Patient_ID_Source, Patient_Death_Indicator, C_Death, C_Death_Source, C_BioSense_ID, C_Processed_BioSense_ID, Patient_Zip, C_Patient_County, C_Patient_County_Source, Diagnosis_Code, Diagnosis_Description, Diagnosis_Segment, Diagnosis_Combo, Diagnosis_Type
							, Diagnosis_Priority, Discharge_Disposition, Triage_Notes, Administrative_Sex, Race_Code, Race_Description, Race_Segment, Ethnicity_Code, Ethnicity_Description, Ethnicity_Segment, Str_Initial_Temp, Initial_Temp, Initial_Temp_Units, Alternate_Visit_ID, Procedure_Code
							, Procedure_Description, Procedure_Segment, Procedure_Combo, Str_Onset_Date, Onset_Date, Clinical_Impression, Problem_List_Code, Problem_List_Description, Problem_List_Segment, Problem_List_Combo, Str_Initial_Pulse_Oximetry, Initial_Pulse_Oximetry
							, Initial_Pulse_Oximetry_Units, Initial_Acuity_Code, Initial_Acuity_Description, Initial_Acuity_Segment, Initial_Acuity_Combo, Servicing_Facility, Unique_Physician_Identifier, Unique_Physician_Identifier_Assigning_Authority, Provider_Type_Code, Provider_Type_Description
							, Provider_Type_Segment, Provider_Type_Combo, Patient_City, Patient_State, Patient_Country, Hospital_Unit_Code, Hospital_Unit_Description, Hospital_Unit_Segment, Str_Height, Height, Height_Units, Str_Weight, Weight, Weight_Units, Str_Body_Mass_Index, Body_Mass_Index
							, Smoking_Status_Code, Smoking_Status_Description, Smoking_Status_Segment, Str_Systolic_Blood_Pressure, Systolic_Blood_Pressure, Systolic_Blood_Pressure_Units, Str_Diastolic_Blood_Pressure, Diastolic_Blood_Pressure, Diastolic_Blood_Pressure_Units							
							, Str_Systolic_Diastolic_Blood_Pressure, Systolic_Diastolic_Blood_Pressure, Systolic_Diastolic_Blood_Pressure_Units, Event_Type_Code, Insurance_Coverage, Insurance_Company_ID, Medication_List, Medication_Code, Medication_Description, Medication_Segment
							, Medication_Combo, Previous_Hospital_Unit, Pregnancy_Status_Code, Pregnancy_Status_Description, Pregnancy_Status_Segment, Hospital_Service, Admit_Source, Ambulatory_Status, Admission_Type, Travel_History, Discharge_Instructions, Initial_Evaluation_Note
							, C_MFT_Patient_Class, Time_Zone, Create_Processed_Date_Time, Update_Processed_Date_Time, Assigned_Patient_Location, Legacy_Flag, Legacy_Row_Number, C_Biosense_Facility_ID
							-- Left out Update_Essence, C_BMI
						FROM [dbo].[#TempTable] WHERE [Message_ID] IN (SELECT Message_ID FROM @Exceptions_Count)

					SELECT  @Exception_Count = @@ROWCOUNT	

			IF (@Debug = 1)
				PRINT 'Mark #7'

					INSERT INTO dbo.PH_Staging_Exceptions_Reason (Message_ID, Exceptions_Reason_Code, Exception_Date)
					  SELECT Message_ID, Reason_Code, SYSDATETIME() FROM @Exceptions_Count
						
					-- Remove from Temp Table						
					DELETE [dbo].[#TempTable] WHERE [Message_ID] IN (SELECT Message_ID FROM @Exceptions_Count)
				  END

			IF (@Debug = 1)
				PRINT 'Mark #8'

			-- Copy to processed table
			INSERT [PH_Staging_Processed]
			([Administrative_Sex]
			,[Admission_Type]
			,[Admit_Date_Time]
			,[Admit_Reason_Code]
			,[Admit_Reason_Combo]
			,[Admit_Reason_Description]
			,[Admit_Reason_Segment]
			,[Admit_Source]
			,[Age_Calculated]
			,[Age_Reported]
			,[Age_Units_Calculated]
			,[Age_Units_Reported]
			,[Alternate_Visit_ID]
			,[Ambulatory_Status]
			,[Arrived_Date_Time]
			,[Assigned_Patient_Location]
			,[Birth_Date_Time]
			,[Body_Mass_Index]
			,[C_Biosense_Facility_ID]
			,[C_BioSense_ID]
			,[C_BMI]
			,[C_Chief_Complaint]
			,[C_Chief_Complaint_Source]
			,[C_Death]
			,[C_Death_Source]
			,[C_Facility_ID]
			,[C_Facility_ID_Source]
			,[C_FacType_Patient_Class]
			,[C_MFT_Patient_Class]
			,[C_Patient_Age]
			,[C_Patient_Age_Source]
			,[C_Patient_Age_Units]
			,[C_Patient_Age_Years]
			,[C_Patient_Class]
			,[C_Patient_Class_Source]
			,[C_Patient_County]
			,[C_Patient_County_Source]
			,[C_Processed_BioSense_ID]
			,[C_Processed_Facility_ID]
			,[C_Unique_Patient_ID]
			,[C_Unique_Patient_ID_Source]
			,[C_Visit_Date]
			,[C_Visit_Date_Source]
			,[C_Visit_Date_Time]
			,[Channel_Name]
			,[Chief_Complaint_Code]
			,[Chief_Complaint_Combo]
			,[Chief_Complaint_Segment]
			,[Chief_Complaint_Text]
			,[Chief_Complaint_Type]
			,[Clinical_Impression]
			,[Create_Processed_Date_Time]
			,[Create_Raw_Date_Time]
			,[Death_Date_Time]
			,[Diagnosis_Code]
			,[Diagnosis_Combo]
			,[Diagnosis_Date_Time]
			,[Diagnosis_Description]
			,[Diagnosis_Priority]
			,[Diagnosis_Segment]
			,[Diagnosis_Type]
			,[Diastolic_Blood_Pressure]
			,[Diastolic_Blood_Pressure_Units]
			,[Discharge_Date_Time]
			,[Discharge_Disposition]
			,[Discharge_Instructions]
			,[Ethnicity_Code]
			,[Ethnicity_Description]
			,[Ethnicity_Segment]
			,[Event_Type_Code]
			,[Facility_Type_Code]
			,[Facility_Type_Description]
			,[Facility_Type_Segment]
			,[Feed_Name]
			,[First_Patient_ID]
			,[First_Patient_ID_Assigning_Authority]
			,[First_Patient_ID_Assigning_Facility]
			,[First_Patient_ID_Type_Code]
			,[Height]
			,[Height_Units]
			,[Hospital_Service]
			,[Hospital_Unit_Code]
			,[Hospital_Unit_Description]
			,[Hospital_Unit_Segment]
			,[Initial_Acuity_Code]
			,[Initial_Acuity_Combo]
			,[Initial_Acuity_Description]
			,[Initial_Acuity_Segment]
			,[Initial_Evaluation_Note]
			,[Initial_Pulse_Oximetry]
			,[Initial_Pulse_Oximetry_Units]
			,[Initial_Temp]
			,[Initial_Temp_Units]
			,[Insurance_Company_ID]
			,[Insurance_Coverage]
			,[Legacy_Flag]
			,[Legacy_Row_Number]
			,[Medical_Record_Number]
			,[Medical_Record_Number_Assigning_Authority]
			,[Medical_Record_Number_Assigning_Facility]
			,[Medication_Code]
			,[Medication_Combo]
			,[Medication_Description]
			,[Medication_List]
			,[Medication_Segment]
			,[Message_Control_ID]
			,[Message_Date_Time]
			,[Message_ID]  -- Uses Default value to get next sequence value
			,[Message_Profile_ID]
			,[Message_Structure]
			,[Message_Type]
			,[Observation_Date_Time]
			,[Onset_Date]
			,[Patient_Account_Number]
			,[Patient_City]
			,[Patient_Class_Code]
			,[Patient_Country]
			,[Patient_Death_Indicator]
			,[Patient_State]
			,[Patient_Zip]
			,[Pregnancy_Status_Code]
			,[Pregnancy_Status_Description]
			,[Pregnancy_Status_Segment]
			,[Previous_Hospital_Unit]
			,[Problem_List_Code]
			,[Problem_List_Combo]
			,[Problem_List_Description]
			,[Problem_List_Segment]
			,[Procedure_Code]
			,[Procedure_Combo]
			,[Procedure_Date_Time]
			,[Procedure_Description]
			,[Procedure_Segment]
			--,[Processed_ID]
			,[Processing_ID]
			,[Provider_Type_Code]
			,[Provider_Type_Combo]
			,[Provider_Type_Description]
			,[Provider_Type_Segment]
			,[Race_Code]
			,[Race_Description]
			,[Race_Segment]
			,[Receiving_Application]
			,[Receiving_Facility]
			,[Recorded_Date_Time]
			,[Sending_Application]
			,[Sending_Facility_ID]
			,[Sending_Facility_ID_Source]
			,[Servicing_Facility]
			,[Site_ID]
			,[Smoking_Status_Code]
			,[Smoking_Status_Description]
			,[Smoking_Status_Segment]
			,[Str_Admit_Date_Time]
			,[Str_Age_Calculated]
			,[Str_Age_Reported]
			,[Str_Arrived_Date_Time]
			,[Str_Birth_Date_Time]
			,[Str_Body_Mass_Index]
			,[Str_Death_Date_Time]
			,[Str_Diagnosis_Date_Time]
			,[Str_Diastolic_Blood_Pressure]
			,[Str_Discharge_Date_Time]
			,[Str_Height]
			,[Str_Initial_Pulse_Oximetry]
			,[Str_Initial_Temp]
			,[Str_Message_Date_Time]
			,[Str_Observation_Date_Time]
			,[Str_Onset_Date]
			,[Str_Procedure_Date_Time]
			,[Str_Recorded_Date_Time]
			,[Str_Systolic_Blood_Pressure]
			,[Str_Systolic_Diastolic_Blood_Pressure]
			,[Str_Weight]
			,[Systolic_Blood_Pressure]
			,[Systolic_Blood_Pressure_Units]
			,[Systolic_Diastolic_Blood_Pressure]
			,[Systolic_Diastolic_Blood_Pressure_Units]
			,[Time_Zone]
			,[Travel_History]
			,[Treating_Facility_ID]
			,[Triage_Notes]
			,[Trigger_Event]
			,[Unique_Physician_Identifier]
			,[Unique_Physician_Identifier_Assigning_Authority]
			,[Update_Essence]
			,[Update_Processed_Date_Time]
			,[Version_ID]
			,[Visit_ID]
			,[Visit_ID_Assigning_Authority]
			,[Visit_ID_Assigning_Facility]
			,[Weight]
			,[Weight_Units]
			)
			SELECT 
			[Administrative_Sex]
			,[Admission_Type]
			,[Admit_Date_Time]
			,[Admit_Reason_Code]
			,[Admit_Reason_Combo]
			,[Admit_Reason_Description]
			,[Admit_Reason_Segment]
			,[Admit_Source]
			,[Age_Calculated]
			,[Age_Reported]
			,[Age_Units_Calculated]
			,[Age_Units_Reported]
			,[Alternate_Visit_ID]
			,[Ambulatory_Status]
			,[Arrived_Date_Time]
			,[Assigned_Patient_Location]
			,[Birth_Date_Time]
			,[Body_Mass_Index]
			,[C_Biosense_Facility_ID]
			,[C_BioSense_ID]
			,[C_BMI]
			,[C_Chief_Complaint]
			,[C_Chief_Complaint_Source]
			,[C_Death]
			,[C_Death_Source]
			,[C_Facility_ID]
			,[C_Facility_ID_Source]
			,[C_FacType_Patient_Class]
			,[C_MFT_Patient_Class]
			,[C_Patient_Age]
			,[C_Patient_Age_Source]
			,[C_Patient_Age_Units]
			,[C_Patient_Age_Years]
			,[C_Patient_Class]
			,[C_Patient_Class_Source]
			,[C_Patient_County]
			,[C_Patient_County_Source]
			,[C_Processed_BioSense_ID]
			,[C_Processed_Facility_ID]
			,[C_Unique_Patient_ID]
			,[C_Unique_Patient_ID_Source]
			,[C_Visit_Date]
			,[C_Visit_Date_Source]
			,[C_Visit_Date_Time]
			,[Channel_Name]
			,[Chief_Complaint_Code]
			,[Chief_Complaint_Combo]
			,[Chief_Complaint_Segment]
			,[Chief_Complaint_Text]
			,[Chief_Complaint_Type]
			,[Clinical_Impression]
			,[Create_Processed_Date_Time]
			,[Create_Raw_Date_Time]
			,[Death_Date_Time]
			,[Diagnosis_Code]
			,[Diagnosis_Combo]
			,[Diagnosis_Date_Time]
			,[Diagnosis_Description]
			,[Diagnosis_Priority]
			,[Diagnosis_Segment]
			,[Diagnosis_Type]
			,[Diastolic_Blood_Pressure]
			,[Diastolic_Blood_Pressure_Units]
			,[Discharge_Date_Time]
			,[Discharge_Disposition]
			,[Discharge_Instructions]
			,[Ethnicity_Code]
			,[Ethnicity_Description]
			,[Ethnicity_Segment]
			,[Event_Type_Code]
			,[Facility_Type_Code]
			,[Facility_Type_Description]
			,[Facility_Type_Segment]
			,[Feed_Name]
			,[First_Patient_ID]
			,[First_Patient_ID_Assigning_Authority]
			,[First_Patient_ID_Assigning_Facility]
			,[First_Patient_ID_Type_Code]
			,[Height]
			,[Height_Units]
			,[Hospital_Service]
			,[Hospital_Unit_Code]
			,[Hospital_Unit_Description]
			,[Hospital_Unit_Segment]
			,[Initial_Acuity_Code]
			,[Initial_Acuity_Combo]
			,[Initial_Acuity_Description]
			,[Initial_Acuity_Segment]
			,[Initial_Evaluation_Note]
			,[Initial_Pulse_Oximetry]
			,[Initial_Pulse_Oximetry_Units]
			,[Initial_Temp]
			,[Initial_Temp_Units]
			,[Insurance_Company_ID]
			,[Insurance_Coverage]
			,[Legacy_Flag]
			,[Legacy_Row_Number]
			,[Medical_Record_Number]
			,[Medical_Record_Number_Assigning_Authority]
			,[Medical_Record_Number_Assigning_Facility]
			,[Medication_Code]
			,[Medication_Combo]
			,[Medication_Description]
			,[Medication_List]
			,[Medication_Segment]
			,[Message_Control_ID]
			,[Message_Date_Time]
			,[Message_ID]  -- Uses Default value to get next sequence value
			,[Message_Profile_ID]
			,[Message_Structure]
			,[Message_Type]
			,[Observation_Date_Time]
			,[Onset_Date]
			,[Patient_Account_Number]
			,[Patient_City]
			,[Patient_Class_Code]
			,[Patient_Country]
			,[Patient_Death_Indicator]
			,[Patient_State]
			,[Patient_Zip]
			,[Pregnancy_Status_Code]
			,[Pregnancy_Status_Description]
			,[Pregnancy_Status_Segment]
			,[Previous_Hospital_Unit]
			,[Problem_List_Code]
			,[Problem_List_Combo]
			,[Problem_List_Description]
			,[Problem_List_Segment]
			,[Procedure_Code]
			,[Procedure_Combo]
			,[Procedure_Date_Time]
			,[Procedure_Description]
			,[Procedure_Segment]
			--,[Processed_ID]
			,[Processing_ID]
			,[Provider_Type_Code]
			,[Provider_Type_Combo]
			,[Provider_Type_Description]
			,[Provider_Type_Segment]
			,[Race_Code]
			,[Race_Description]
			,[Race_Segment]
			,[Receiving_Application]
			,[Receiving_Facility]
			,[Recorded_Date_Time]
			,[Sending_Application]
			,[Sending_Facility_ID]
			,[Sending_Facility_ID_Source]
			,[Servicing_Facility]
			,[Site_ID]
			,[Smoking_Status_Code]
			,[Smoking_Status_Description]
			,[Smoking_Status_Segment]
			,[Str_Admit_Date_Time]
			,[Str_Age_Calculated]
			,[Str_Age_Reported]
			,[Str_Arrived_Date_Time]
			,[Str_Birth_Date_Time]
			,[Str_Body_Mass_Index]
			,[Str_Death_Date_Time]
			,[Str_Diagnosis_Date_Time]
			,[Str_Diastolic_Blood_Pressure]
			,[Str_Discharge_Date_Time]
			,[Str_Height]
			,[Str_Initial_Pulse_Oximetry]
			,[Str_Initial_Temp]
			,[Str_Message_Date_Time]
			,[Str_Observation_Date_Time]
			,[Str_Onset_Date]
			,[Str_Procedure_Date_Time]
			,[Str_Recorded_Date_Time]
			,[Str_Systolic_Blood_Pressure]
			,[Str_Systolic_Diastolic_Blood_Pressure]
			,[Str_Weight]
			,[Systolic_Blood_Pressure]
			,[Systolic_Blood_Pressure_Units]
			,[Systolic_Diastolic_Blood_Pressure]
			,[Systolic_Diastolic_Blood_Pressure_Units]
			,[Time_Zone]
			,[Travel_History]
			,[Treating_Facility_ID]
			,[Triage_Notes]
			,[Trigger_Event]
			,[Unique_Physician_Identifier]
			,[Unique_Physician_Identifier_Assigning_Authority]
			,[Update_Essence]
			,[Update_Processed_Date_Time]
			,[Version_ID]
			,[Visit_ID]
			,[Visit_ID_Assigning_Authority]
			,[Visit_ID_Assigning_Facility]
			,[Weight]
			,[Weight_Units]
			FROM #TempTable

--			SELECT @Imported_Rows = @@ROWCOUNT, @MyRC = @@ERROR, @Process_Status = 'Success', @Process_Msg = 'No Errors'
			SELECT @Imported_Rows = @@ROWCOUNT, @MyRC = @@ERROR, @Process_Status = 'Success', @Process_Msg = 'No Errors' + ' - Sessions = ' + CAST(@N_Parallel AS VARCHAR(20)) + ' TaskID = ' +CAST(@Task_Number AS VARCHAR(20))

			IF (@Debug = 1)
				PRINT 'Mark #9'

			IF (@Debug = 1)
				PRINT 'Updating Archive Table'
			UPDATE 	[Legacy_BioSense].[dbo].[PH_Stage_1_Archive] SET Convert_Status = 'Y' 
				FROM [Legacy_BioSense].[dbo].[PH_Stage_1_Archive] a
					JOIN @Control_Table t ON a.[Row_Number] = t.[Row_Number]

			-- Bob W. - Not using a control table here
			--UPDATE [Legacy_Process_Control] SET [LastProcessedID] = @EndID
			--WHERE [Site_ID] = @Site_ID

			SET @MyRC = 0	

		END TRY
		BEGIN CATCH
			IF (@Debug = 1)
			  BEGIN
				PRINT 'Mark #10'
				SELECT * FROM @Control_Table
			  END

			SELECT  @Imported_Rows	= @@ROWCOUNT, 
					@MyRC			= @@ERROR, 
					@Process_Status = 'Error',
					@Process_Msg	= 'Sessions = '  + CAST(@N_Parallel AS VARCHAR(20)) + ' TaskID = ' +CAST(@Task_Number AS VARCHAR(20)) + ' - Error: ' + LTRIM(RTRIM(STR(@MyRC))) + '  Line #: ' + LTRIM(RTRIM(STR(ERROR_LINE()))) + '  - ' + ERROR_MESSAGE()  

			-- @ArchiveTable		
--			UPDATE [Legacy_Msg_Tracking_XX] SET Imported = 'E' WHERE [Row_Number]=@MyRow

			INSERT [dbo].[Legacy_Error_Log](Processed_Table_Name, Error_Time, Row_Number, Error, Error_Message)
				VALUES(@ArchiveTable,GETDATE(),@MyRow,LTRIM(RTRIM(STR(@MyRC))),@Process_Msg)


		END CATCH


	SET @Import_Stop = SYSDATETIME()

	IF (@Debug = 1)
		PRINT 'Mark #11'

	INSERT [Legacy_Process_Log] (Processed_Table_Name, Legacy_Import_Start_DateTime, Legacy_Import_End_DateTime, Legacy_Imported_Records, Legacy_Exceptioned_Records, Process_Status, Log_Description)
		SELECT  @LegacyTable,
				@Import_Start,
				@Import_Stop,
				@Imported_Rows,
				@Exception_Count,
				@Process_Status,
				@Process_Msg

	
	IF (@Debug > 0)
			SELECT  @LegacyTable,
				@Import_Start,
				@Import_Stop,
				@Imported_Rows,
				@Process_Status,
				@Process_Msg

	--DROP TABLE #Process_Records;
	DROP TABLE #TempTable
	--DROP TABLE #TempTable2

	RETURN(@MyRC)
END


GO


